;; http/REST client for org-mode
(use-package verb
  :config
  ;; too long for certain servers
  ;; (setq verb-base-headers `(("User-Agent" . ,(emacs-version))))
  (setq verb-auto-kill-response-buffers t))

(use-package gnuplot)

;; a major mode for keeping notes,authoring documents, computational notebooks,
;; literate programming, maintaining to-do lists, planning projects, and more
(use-package org
  ;; it is a built-in package, do not try to fetch it
  :straight (:type built-in)
  :init
  ;; export backends
  (setq org-export-backends '(latex latex-cv awesomecv beamer html org md ascii))
  :bind
  (("C-c a" . org-agenda)
   ("C-c c" . org-capture)
   ("C-c l" . org-store-link)
   :map org-mode-map
   ("C-c SPC" . org-table-blank-field))
  :config
  ;; convoluted key bindings --- a keymap on a prefix within another keymap
  (define-key org-mode-map (kbd "C-c C-r") verb-command-map)
  ;; root directory for org files
  (setq org-directory (expand-file-name "~/Documents/org"))
  ;; agenda setup
  (setq org-agenda-custom-commands
        '(("f" occur-tree "FIXME")))
  ;; enable languages for babel source blocks
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t)
     (python . t)
     (clojure . t)
     (scheme .t)
     ;; (http . t)
     ;; (restclient . t)
     (verb . t)
     (plantuml . t)))
  ;; source code blocks - default headers
  (setq org-babel-default-header-args
        '((:session . "none")
          (:results . "value verbatim replace")
          (:exports . "code")
          (:cache . "no")
          (:noweb . "no")
          (:hlines . "no")
          (:tangle . "no")))
  ;; set the default backend for evaluation of Clojure source code blocks
  (setq org-babel-clojure-backend 'babashka)
  ;; disable source code block evaluation confirmation
  (setq org-confirm-babel-evaluate nil)
  ;; TODO states
  (setq org-todo-keywords
        '((sequence
           "TODO(t)" "INPROGRESS(i)" "PAUSED(p)" "|" "DONE(d)" "CANCELLED(c)")))
  (setq org-todo-keyword-faces
        '(("INPROGRESS" . "dark orange")
          ("CANCELLED" . "light blue")
          ("DONE" . "light green")))
  (setq org-enforce-todo-dependencies t)
  (setq org-log-into-drawer t)
  ;; schedules and deadlines
  (setq org-log-redeadline t)
  ;; LaTeX fragments preview
  (plist-put org-format-latex-options :scale 1.8)
  ;; PlantUML jar path
  (setq org-plantuml-jar-path
        (expand-file-name "~/Programs/plantuml/plantuml.jar"))
  (setq org-adapt-indentation t))

;; pomodoro timer for org-mode
(use-package org-pomodoro
  :config
  (setq org-pomodoro-format "%s"))

;; custom bullets for outline headers
(use-package org-bullets
  :config
  (setq org-bullets-bullet-list '("○" "○" "○" "○"))
  :hook
  (org-mode . org-bullets-mode))


;; a plain-text personal knowledge management system
(use-package org-roam
  :diminish
  :custom
  (org-roam-directory (file-truename "~/Documents/org/roam"))
  :init
  (setq org-roam-v2-ack t)
  :config
  (org-roam-setup)
  (require 'org-roam-protocol)
  (setq org-roam-database-connector 'sqlite-builtin)
  :bind (("C-c n b" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ("C-c n d" . org-roam-dailies-capture-today)
         ("C-c n t" . org-roam-dailies-goto-today)))

;; a web frontend for org-roam
(use-package org-roam-ui
  :bind (("C-c n u" . org-roam-ui-open)))


;; a spaced-repetition (flashcards) system for org-mode
(use-package org-fc
  :straight
  (org-fc
   :type git
   :host nil
   :repo "https://git.sr.ht/~l3kn/org-fc"
   :files (:defaults "awk" "demo.org"))
  :custom
  (org-fc-directories '("~/Documents/org/flashcards"))
  :config
  (require 'org-fc-hydra)
  :bind
  ("C-c f" . org-fc-hydra/body))


;;; manual customizations

;; toggle advice: recenter the org buffer vertically after filling the paragraph
;; (M-q)
(defun org/center (&optional arg1 arg2)
  "Recenters the org buffer vertically and then tries to remove
horizontal shift to the right, that may appear with the input of
the long line.

It's purpose is to be used in advice to the `org-fill-paragraph',
so it takes the same number of parameters and ignores them."
  (progn
    (recenter)
    (backward-sentence)
    (forward-sentence)))

(defun org/autocenter ()
  (interactive)
  (if (advice-member-p 'org/center 'org-fill-paragraph)
      (progn
        (advice-remove 'org-fill-paragraph #'org/center)
        (message "Autocentering deactivated"))
    (progn
      (advice-add 'org-fill-paragraph :after #'org/center)
      (message "Autocentering activated"))))
