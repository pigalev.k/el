# hello-elisp

Exploring fundamentals of Emacs Lisp and it's various APIs.

## Sources

- Emacs Lisp Reference Manual
  https://www.gnu.org/software/emacs/manual/html_node/elisp/index.html
- `use-package` User Manual
  https://www.gnu.org/software/emacs/manual/html_node/use-package/index.html
- Org Element API https://orgmode.org/worg/dev/org-element-api.html

## Getting Started

Start Emacs, open files, eval code.
