(load-file "../common.el")


;; generate an abstract syntax tree (AST) from an Org buffer or object at
;; point


(defun walk (f tree)
  "Walks the TREE, replacing it's leaf elements with results of
applying F to them."
  (cond ((null tree) tree)
        ((atom (car tree)) (cons (funcall f (car tree))
                                 (walk f (cdr tree))))
        (:else (cons (walk f (car tree))
                     (walk f (cdr tree))))))

(defun fold (f combine tree)
  ""
  (cond ((null tree) tree)
        ((atom (car tree)) (funcall combine
                                    (funcall f (car tree))
                                    (fold f combine (cdr tree))))
        (:else (funcall combine
                        (fold f combine (car tree))
                        (fold f combine (cdr tree))))))

(setq tree-1 '((a x) (b c (d)) e))
(walk #'symbol-name tree-1)
(walk #'identity tree-1)
(fold #'identity #'cons tree-1)


;;;; parsing

(setq simple-data (with-current-buffer
                      (find-file-noselect "../../resources/simple.org")
                    (org-element-parse-buffer)))

(comment

 ;; parsing globally

 simple-data
 (walk #'car simple-data)

 (with-current-buffer (find-file-noselect "../../resources/empty.org")
   (org-element-parse-buffer))

 ;; parsing locally

 (with-current-buffer (find-file-noselect "../../resources/simple.org")
   (goto-char (point-min))
   (org-element-at-point))

 (with-current-buffer (find-file-noselect "../../empty.org")
   (goto-char (point-min))
   (org-element-at-point))

)


;;;; accessors

(comment

 (setq simple-headline (car (org-element-contents org-simple-contents)))

 (org-element-type simple-headline)
 (org-element-property :raw-value simple-headline)
 (org-element-contents simple-headline)

)
