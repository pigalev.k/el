;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; delays and forcing

;; prior art: (require 'thunk) --- caches forced values

(defmacro delay (&rest body)
  "Constructs an object representing delayed computation of
BODY, that is evaluated each time when this object is forced."
  `(cons 'delay (lambda () ,@body)))

(defun delay? (x)
  "Returns true if X is an object representing delayed
computation."
  (when (listp x)
    (eq (car x) 'delay)))

(defun force (x)
  "If X is a delayed computation object, forces it (performs the
delayed computation) and returns the value, else returns X
itself."
  (if (delay? x)
      (funcall (cdr x))
    x))

(comment

 (macroexpand-all '(lambda (x) (* x x)))
 (macroexpand-all '(delay (+ 1 2)))

 (setq d1 (delay (print "forced")
                 (+ 1 2)))
 (delay? d1)
 (delay? 1)

 (force d1)
 (force 1)

)


;; seqs (lazy sequences)

;; prior art: (require 'seq)

(defmacro seq-cons (head tail)
  "Creates a lazy seq with HEAD as its first element and TAIL as the
rest."
  `(cons 'seq (cons (delay ,head) (delay ,tail))))

(defun seq? (x)
  "Returns true if X is a seq."
  (when (listp x)
    (eq 'seq (car x))))

(defun empty? (seq)
  "Returns true if SEQ is empty. Works on lists as well."
  (null seq))

(defun first (seq)
  "Forces and returns the first element of SEQ."
  (force (cadr seq)))

(defun rest (seq)
  "Forces and returns the rest of SEQ."
  (force (cddr seq)))

(defun seq (list)
  "Returns a new seq created from LIST."
  (if (empty? list)
      nil
    (seq-cons (car list)
              (seq (cdr list)))))

(comment

 (setq s1 (seq-cons 1 (seq-cons 2 (seq-cons (+ 1 2) nil))))
 s1

 (seq? s1)
 (seq? 1)
 (seq? nil)

 (empty? s1)
 (empty? nil)

 (first s1)
 (rest s1)
 (first (rest s1))

)


;; seq manipulation functions

(defun s/into (list seq)
  "Returns LIST with all elements of SEQ added to it."
  (if (empty? seq)
      list
    (s/into (cons (first seq) list)
          (rest seq))))

(defun s/count (seq)
  "Returns the count of elements in SEQ."
  (if (empty? seq)
      0
    (1+ (s/count (rest seq)))))

(defun s/nth (seq n)
  "Returns the nth element of SEQ."
  (if (= n 0)
      (first seq)
    (s/nth (rest seq) (1- n))))

(defun s/map (f &rest seqs)
  "Returns a new seq created by applying F to corresponding elements
of SEQS."
  (if (empty? (car seqs))
      nil
    (seq-cons (apply f (mapcar #'first seqs))
              (apply #'s/map f (mapcar #'rest seqs)))))

(defun s/filter (pred seq)
  "Returns a seq of elements of SEQ for which PRED returns true."
  (cond
   ((empty? seq) seq)
   ((funcall pred (first seq)) (seq-cons (first seq)
                                         (s/filter pred (rest seq))))
   (:else (s/filter pred (rest seq)))))

(defun s/reduce (f init seq)
  "Reduces SEQ to a value applying F to pairs of seq element and
result of the previous application, starting with INIT."
  (if (empty? seq)
      init
    (s/reduce f (funcall f init (first seq)) (rest seq))))

(defun s/foreach (f seq)
  "Applies F to each element of SEQ for side effects. Returns nil."
  (unless (empty? seq)
    (funcall f (first seq))
    (s/foreach f (rest seq))))

(defun s/show (seq)
  "Returns SEQ in human-readable form (as a list)."
  (reverse (s/into '() seq)))

(defun s/range (low high)
  "Returns a seq of integers between LOW and HIGH (inclusive)."
  (if (> low high)
      nil
    (seq-cons low (s/range (1+ low) high))))

(defun s/take (n seq)
  "Returns a seq with (at most) first N elements of SEQ."
  (if (or (empty? seq)
          (= 0 n))
      nil
    (seq-cons (first seq)
              (s/take (1- n) (rest seq)))))

(defun s/drop (n seq)
  "Returns SEQ without first N elements."
  (if (or (empty? seq)
          (= 0 n))
      seq
    (s/drop (1- n) (rest seq))))

(defun s/iterate (f x)
  "Returns a seq of X, (F X), (F (F X)) and so on."
  (labels ((iteration (first-call f x)
                      (if first-call
                          (seq-cons x (iteration nil f x))
                        (let ((value (funcall f x)))
                          (seq-cons value (iteration nil f value))))))
    (iteration t f x)))

(defun s/interleave (seq1 seq2)
  "Returns a seq of alternating elements of SEQ1 and SEQ2."
  (if (empty? seq1)
      seq2
    (seq-cons (first seq1)
              (s/interleave seq2 (rest seq1)))))

(defun s/repeat (x)
  "Returns a seq of Xs."
  (seq-cons x (s/repeat x)))

(comment

 (setq s2 (seq '(1 2 3 4 5)))
 (s/show s2)
 (first s2)
 (first (rest s2))

 (s/into '() s2)
 (s/into '(0 -1) s2)
 (s/count s2)
 (s/nth s2 1)

 (setq s3 (s/map #'+ s2 s2))
 (s/show s3)

 (s/show
  (s/filter (lambda (x) (= 0 (mod x 2))) s2))

 (s/reduce #'+ 0 (s/range 1 10))

 (setq s4 (seq '("a" "b" "c" "d" "e")))
 (s/foreach #'insert s4)

 (s/show
  (s/range 1 10))

 (s/show
  (s/take 3 (s/drop 2 (s/range 1 10))))

 (s/show
  (s/take 5 (s/iterate #'1- 0)))

 (setq s5 (s/iterate #'1- 0))
 (setq s6 (s/iterate #'1+ 0))
 (s/show
  (s/take 10 (s/interleave s5 s6)))

 (s/show
  (s/take 10 (s/repeat :a)))

)


;; infinite seqs

(defun integers-starting-from (n)
  "Returns an (infinite!) seq of integers starting from N."
  (seq-cons n (integers-starting-from (1+ n))))

(setq integers (integers-starting-from 0))

(defun rand-update (x)
  "Returns next pseudorandom number for given pseudorandom number X."
  (mod (+ 74 (* x 75))
       (1+ (expt 2 16))))

(defun random-seq (seed)
  "Returns a seq of pseudorandom integers starting with SEED."
  (labels ((random-numbers-from (random-number)
                                (seq-cons random-number
                                          (s/map #'rand-update
                                                 (random-numbers-from
                                                  random-number)))))
    (random-numbers-from seed)))

(comment

 (setq s8 (s/take 10 integers))

 (s/show s8)

 (s/show
  (s/map #'+ s8 s8))

 (s/show
  (s/map #'* s8 (s/repeat 10)))

 (s/show
  (s/take 10 (random-seq 0)))

)
