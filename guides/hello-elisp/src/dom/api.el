(require 'dom)


;; DOM trees


(comment

 ;; data abstraction (constructor and accessors)

 ;; create DOM tree
 (setq node (dom-node 'div '((class . "foo") (data-foo . "123"))
                      "Text child"
                      (dom-node 'span nil "Node child")
                      (dom-node 'span '((id . "quux")) "Another node child")
                      (dom-node 'p '((class . "bar") (style . "color: white"))
                                "And yet another node child")
                      "Another text child"))

 ;; generic accessors
 (dom-tag node)
 (dom-attributes node)
 (dom-children node)

 ;; specialized accessors
 (dom-non-text-children node)
 (dom-attr node 'id)
 (dom-text node)
 (dom-texts node)
 (dom-strings node)
 (dom-parent node (car (dom-by-id node "quux")))
 (dom-previous-sibling node (car (dom-by-id node "quux")))

 ;; node search
 (dom-child-by-tag node 'span)
 (dom-by-tag node 'span)
 (dom-by-id node "quux")
 (dom-by-class node "bar")
 (dom-by-style node "color")
 (dom-elements node 'class "bar")
 (dom-search node (lambda (el) (eq (dom-tag el) 'p)))

 ;; mutators
 (dom-set-attribute node 'id "foo123")
 (dom-remove-attribute node 'data-foo)
 (dom-remove-node node (car (dom-by-id node "quux")))
 (dom-append-child (car (dom-by-id node "quux"))
                   (dom-node 'a '((href . "http://example.com"))))
 (dom-add-child-before (car (dom-by-id node "quux"))
                       (dom-node 'a '((href . "http://example.com"))))


 ;; rendering

 ;; pretty-print the tree
 (dom-pp node)

 ;; format the tree as HTML
 (dom-print node t)

)
