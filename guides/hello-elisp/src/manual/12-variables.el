;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; a variable is a name used in a program to stand for a value; variables are
;; represented by symbols --- variable name is a symbol name, and the variable's
;; value is stored in the symbol's value cell


;;;; global variables

;; a global variable has one value at a time throughout the system and stays the
;; same until changed; it has an infinite extent

(comment

 (setq x '(a b))
 x
 (setq x 4)
 x

)


;;;; variables that never change

;; certain symbols always evaluate to themselves (constants), and their values
;; cannot be changed

(comment

 t
 nil
 :abc

 (setq :abc 500)

 ;; some additional symbols are made read-only for practical reasons
 most-positive-fixnum

)


;;;; local variables

;; a local variable exist only within a certain part of a program, like function
;; arguments, that exist only within the body of the function; it has limited
;; extent

;; a new local binding shadows the previous value (if any), and the previous
;; value will be in effect again when this binding expires; innermost binding is
;; currently in effect at any time (e.g., will be acted on by `setq')

;; the scoping rule determines where the binding takes effect; the default is
;; dynamic scope, and an alternative is lexical scope

(comment

 ;; values are computed before any variables are bound; order of bindings is
 ;; unspecified
 (let ((x 1)
       (y 2))
   (+ x y))

 ;; later bindings have access to earlier ones
 (let* ((x 1)
        (y (1+ x)))
   (+ x y))

 ;; bindings have access to themselves --- variables are bound before values are
 ;; computed; useful only with lexical scope
 (letrec ((f (lambda () f)))
   (funcall f))

 ;; binds dynamically
 (dlet ((x 1)
        (y 2))
   (+ x y))

 ;; requires lexical scope
 (named-let sum ((numbers '(1 2 3 4))
                 (running-sum 0))
   (if numbers
       (sum (cdr numbers) (+ running-sum (car numbers)))
     running-sum))

)


;;;; when a variable is void

;; when its symbol has an unassigned value cell --- it is different from having
;; a `nil' value; access to a void variable signals an error

;; under dynamic scoping rule, the value cell stores the variable's current
;; (local or global) value; under the lexical scoping rule, the value cell only
;; holds the variable's global value (when a variable is lexically bound, the
;; local value is determined by the lexical environment --- variables can have
;; local values even if their symbols' value cells are unassigned)

(comment

 ;; clears only the current binding if a symbol is dynamically bound

 (setq a 1)
 (makunbound 'a)
 a

 (setq x 1)
 (let ((x 2))
   (makunbound 'x)
   x)
 x

 (boundp 'a)
 (boundp 'x)

)


;;;; defining global variables

;; a variable definition is a construct that announces your intention to use a
;; symbol as a global variable

(comment

 ;; initializes only if initially void; marked as special (always dynamically
 ;; bound)
 (defvar var-foo '(1 2 3)
   "The foo global variable.")
 var-foo

 ;; always initializes; does not enforce immutability; marked as special (always
 ;; dynamically bound); marked as risky
 (defconst const-foo '(4 5 6)
   "The foo global constant.")
 const-foo

)


;;;; tips for defining variables robustly

;; no code, just naming conventions, see the manual


;;;; accessing variable values

;; usually just by writing the symbol name

(comment

 ;; symbol value indirection

 (setq a 1)
 (setq b 2)

 (let ((a 'b))
   (symbol-value 'a))

 (let ((a 'b))
   (symbol-value a))

)


;;;; setting variable values

(comment

 (setq a 1)
 a
 (set 'a 123)
 a

 ;; for user options
 (defcustom my-var 1 "My var."
   :type 'number
   :set (lambda (var val)
          (set-default var val)
          (message "We set %s to %s" var val)))
 (setopt my-var 2)
 my-var
 (symbol-plist 'my-var)

)


;;;; running a function when a variable is changed

;; the variable watchpoint allows to take some action when a variable changes
;; its value

;; because watchers are attached to symbols, mutating values held by symbol do
;; not trigger watchers; also only dynamic variables can be watched (no big
;; deal, since changes to lexical variables can be traced by inspecting the
;; code within the lexical scope)

(comment

 (defun print-value-watcher (sym newval op buffer)
   "Prints the information about the value change of SYM from its old
value to NEVWAL by OP in BUFFER (if buffer-local)."
   (princ
    (format-message
     "The value of `%s' is changed from `%s' to `%s' by `%s' in `%s'"
     sym (symbol-value sym) newval op buffer)))

 (add-variable-watcher 'a 'print-value-watcher)

 (setq a 2)

 (get-variable-watchers 'a)

 (remove-variable-watcher 'a 'print-value-watcher)

 ;; watchers are stored in symbol metadata
 (symbol-plist 'a)

)


;;;; scoping rules for variable bindings

;; each local binding has a certain scope and extent; scope is from where in
;; source text the binding can be accessed; extent is when the binding exists at
;; the program run time

;; dynamic bindings have dynamic scope (any part of the program can access it)
;; and dynamic extent (the binding lasts only while the binding construct is
;; being executed)

;; lexical bindings have lexical scope (only code within the binding construct
;; can access it) and indefinite extent (binding can outlive the binding
;; construct --- e.g., by closure)


;;;; dynamic binding

;; dynamically-bound variable can be accessed from anywhere in program

;; the value is simply stored in the symbol's value cell, and shadowing follows
;; the stack discipline

;;;; proper use of dynamic binding

;; if variable has no global definition, bind it locally before use to isolate
;; changes

;; otherwise, define the variable with `defvar', `defconst' or `defcustom' at
;; top-level in a file and document it

(comment

 ;; special variable (always dynamic, even if lexical binding is enabled)
 (defvar x -99)
 (special-variable-p 'x)

 (defun getx () x)

 ;; dynamically bound
 (dlet ((x 1))
   (getx))

 (getx)

 (defun addx () (setq x (1+ x)))

 (dlet ((x 1))
   (addx)
   (addx))

 (addx)

 ;; navigable, documented global dynamic variable
 case-fold-search


 ;; using `defvar' without value, it is possible to bind a variable dynamically
 ;; just in one file, or in just one part of a file while still binding it
 ;; lexically elsewhere

 (let (_)
   (defvar x1)
   ;; a temporary dynamic binding of x1
   (let ((x1 -99))
     (defun get-dynamic-x1 ()
       x1)))

 ;; a lexical binding of x1
 (let ((x1 'lexical))
   (defun get-lexical-x1 ()
     x1))

 (let (_)
   (defvar x1)
   (let ((x1 'dynamic))
     (list (get-lexical-x1)
           (get-dynamic-x1))))

 (special-variable-p 'x1)

)


;;;; lexical binding

;; a lexically bound variable has lexical scope --- any reference to the
;; variable must be located textually (statically) within the binding construct

;; each binding construct defines a lexical environment, specifying the
;; variables that are bound withing the construct and their local values; on
;; access, the value is looked up in the lexical environment first, then in the
;; symbol's value cell

;; lexical environment is a list of symbol-value pairs (some elements can be
;; symbols --- it means that the symbol's variable is considered dynamically
;; bound); it can be explicitly passed to `eval' as an evaluation context

;; lexical bindings have indefinite extent --- even after a binding construct
;; has finished executing, its lexical environment can be kept in Lisp objects
;; called closures; closure can access bindings from this environment when
;; called

;; allows optimizations, more compatible with concurrency

(comment

 (let ((y 1))
   (+ y 3))

 (defun gety () y)

 ;; error is signaled: void variable y; `gety' was defined outside of `let'
 (let ((y 1))
   (gety))


 (defvar my-ticker nil)
 ;; defines a closure `my-ticker', that closes over lexically bound local
 ;; variable `n' and can access and mutate it; but no other code can
 (let ((n 0))
   (setq my-ticker (lambda ()
                     (setq n (1+ n)))))

 (funcall my-ticker)

)


;;;; using lexical binding

;; set a buffer-local variable `lexical-binding' (e.g., in a file header), or
;; pass the LEXICAL argument to `eval'

;; enabled by default in *scratch*, *ielm*, when evaluating expressions via M-:
;; (`eval-expression')


;;;; converting to lexical binding

;; byte-compiler will warn on reference or assignment to a free non-special
;; variable, unused lexical variable and a special variable used as a function
;; argument


;;;; buffer-local variables

;;;; introduction to buffer-local variables

;; variable bindings that apply only in one buffer; the binding is in effect
;; when that buffer is current

;; a variable can have buffer-local bindings in some buffers but not in other
;; buffers

;; the variable's ordinary binding is called the default binding (usually
;; global); setting variable in a buffer without buffer-local binding sets the
;; default binding

;; note: changing buffers can change bindings

;; a buffer-local variable is permanent if the variable's symbol has a
;; permanent-local property non-nil

;;;; creating and deleting buffer-local bindings

;;;; the default value of a buffer-local variable

(comment

 ;; for current buffer only
 (setq buffer-local-foo :default-value)
 (make-local-variable 'buffer-local-foo)
 buffer-local-foo

 ;; like `make-local-variable' followed by `setq', for each var
 (setq-local buffer-local-bar 12
             buffer-local-quux 345)
 buffer-local-bar

 (local-variable-p 'buffer-local-bar)
 (local-variable-if-set-p 'buffer-local-bar)
 (buffer-local-boundp 'buffer-local-bar (current-buffer))
 (buffer-local-value 'buffer-local-bar (current-buffer))

 (length (buffer-local-variables))
 (mapcar #'car (buffer-local-variables))

 ;; deletes the buffer-local binding, reverting to default value in the current
 ;; buffer
 (kill-local-variable 'buffer-local-bar)
 ;; like switching to Fundamental mode and erasing most of the effects of the
 ;; previous major mode
 (kill-all-local-variables)

 change-major-mode-hook


 ;; automatically buffer-local; makes the variable buffer-local in all buffers
 ;; (including new ones); all buffers start out by sharing the default value,
 ;; but setting the variable creates a buffer-local binding for the current
 ;; buffer --- thus its default value cannot be changed with `setq', only
 ;; with `setq-default'

 (setq auto-buffer-local-foo :default-value)
 (make-variable-buffer-local 'auto-buffer-local-foo)

 ;; the default binding
 auto-buffer-local-foo

 (setq auto-buffer-local-foo :changed-value)
 ;; a new buffer-local binding
 auto-buffer-local-foo

 ;; default value is still here
 (default-boundp 'auto-buffer-local-foo)
 (default-value 'auto-buffer-local-foo)
 ;; changing the default value
 (setq-default auto-buffer-local-foo :new-default-value)
 (set-default 'auto-buffer-local-foo :new-default-value)
 ;; work even if shadowed by a `let'-binding
 (default-toplevel-value 'auto-buffer-local-foo)
 (set-default-toplevel-value 'auto-buffer-local-foo :new-default-value)

 (with-temp-buffer
   ;; default buffer-local binding (not the changed one)
   (princ auto-buffer-local-foo)
   (setq auto-buffer-local-foo :temp-buffer-value)
   ;; new temp buffer-local binding
   auto-buffer-local-foo)

 ;; changed buffer-local binding (not the default one)
 auto-buffer-local-foo


 ;; like `defvar' followed by `make-variable-buffer-local'
 (defvar-local auto-buffer-local-bar 3)

)


;;;; file-local variables

;; a file can specify local variable values; they will be used to create
;; buffer-local bindings for those variables in the buffer visiting that file

(comment

 file-local-variables-alist
 (hack-local-variables)
 before-hack-local-variables-hook

 ;; safety matters

 enable-local-variables
 enable-local-eval

 safe-local-variable-values
 safe-local-eval-forms

 (setq safe-fill-column? (plist-get (symbol-plist 'fill-column)
                                    'safe-local-variable))
 (funcall safe-fill-column? 12)
 (funcall safe-fill-column? "a")
 (safe-local-variable-p 'fill-column 123)

 ;; any variable whose symbol has a non-nil `risky-local-variable' property is
 ;; considered risky; variables named with certain postfixes also considered
 ;; risky
 (risky-local-variable-p 'fill-column)
 (risky-local-variable-p 'org-mode-map)

 ;; and some are simply ignored
 ignored-local-variables
 ignored-local-variable-values


 inhibit-local-variables-regexps
 permanently-enabled-local-variables

)


;;;; directory local variables

;; a directory can specify local variable values for all files in that
;; directory; they will be used to create buffer-local bindings for those
;; variables in buffers visiting any file in that directory

;; there are two methods to do so: by putting the variables in a special file,
;; of by defining a project class for that directory

(comment

 enable-dir-local-variables

 ;; file with local variables for the directory it is in (or subdirectories; the
 ;; deepest file wins)
 dir-locals-file

 (hack-dir-local-variables)
 (dir-locals-set-class-variables 'foo '(org-mode ((buffer-local-foo . bar))))
 (dir-locals-set-directory-class "." 'foo)

 dir-locals-class-alist
 dir-locals-directory-cache

)


;;;; connection local variables

;; connection-local variables can have different values in buffers with a remote
;; connection, depending on the connection

;;;; connection local profiles

;; connection profile is a symbol holding connection-local variables and their
;; values

;;;; applying connection local variables

(comment

 connection-local-profile-alist
 connection-local-criteria-alist


 ;; create connection profiles

 (connection-local-set-profile-variables
  'remote-bash
  '((shell-file-name . "/bin/bash")
    (shell-command-switch . "-c")
    (shell-interactive-switch . "-i")
    (shell-login-switch . "-l")))

 (connection-local-set-profile-variables
  'remote-null-device
  '((null-device . "/dev/null")))


 (connection-local-get-profile-variables
  'remote-bash)


 ;; assign connection profiles to all remote connections identified by criteria

 (connection-local-set-profiles
  '(:application tramp :protocol "ssh" :machine "localhost")
  'remote-bash 'remote-null-device)

 (connection-local-set-profiles
  '(:application tramp :protocol "sudo"
                 :user "root" :machine "localhost")
  'remote-null-device)

 (connection-local-get-profiles '())


 ;; applying connection-local variables

 (hack-connection-local-variables nil)
 (hack-connection-local-variables-apply nil)

 (with-connection-local-application-variables 'my-app
   (princ "processing"))

 connection-local-default-application

 (with-connection-local-variables
  (princ "processing"))

 connection-local-profile-name-for-setq

 (setq-connection-local foo 123)

)


;;;; variable aliases

;; variable aliases are synonyms for variables, for access and modification

(comment

 ;; one of

 (defvaralias 'new-foo 'old-foo)
 (make-obsolete-variable 'old-foo 'new-foo "30.0")
 (define-obsolete-variable-alias 'old-foo 'new-foo "30.0")

 (indirect-variable 'new-foo)

 (setq new-foo 123)

 old-foo
 new-foo

)


;;;; variables with restricted values

;; ordinary Lisp variables can be assigned any Lisp value; but certain variables
;; are not defined in Lisp, but in C, with a type hardcoded

(comment

 ;; boolean-only
 byte-boolean-vars
 ;; integer-only
 (setq undo-limit 1000.0)

)


;;;; generalized variables

;; a generalized variable (or place form) is one of the places where values can
;; be stored using `setf'; the simplest form is a regular variable; CARs and
;; CDRs of lists, elements of arrays, properties of symbols, etc are also such
;; places

;;;; the `setf' macro

;; see the manual for gory details

(comment

 (setf a [1 2 3 4 5])
 (setf l '(1 2 3 4 5))

 (setf (aref a 1) -2)
 (setf (car l) 0)

)


;;;; defining new `setf' forms

(comment

 ;; TODO: return to this if it becomes really needed

)


;;;; multisession variables

;; a facility used to replicate data between sessions

(comment

 ;; holds the value after restart and reevaluation of definitions
 (define-multisession-variable foo-var 0
   "A permanent counter.")


 (defun my-adder (num)
   (interactive "nAdd number: ")
   (setf (multisession-value foo-var)
         (+ (multisession-value foo-var) num))
   (message "The new number is: %s" (multisession-value foo-var)))

 (my-adder 1)


 (multisession-value foo-var)
 (setf (multisession-value foo-var) 1000)


 (setq quux (make-multisession :package "mail"
                               :key "friends"))
 (setf (multisession-value quux) 'everybody)
 (multisession-value quux)


 (list-multisession-values)


 ;; where to store values
 multisession-storage
 multisession-directory

)
