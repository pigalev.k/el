;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; a list is a sequence of zero or more elements (any Lisp objects) made of cons
;; cells; lists can share parts of their structure, and insertion or deletion
;; to/from the list (at head) does not require copying the list


;;;; lists and cons cells

;; a proper list is nil-terminated; its CDR is always a proper list

;; a dotted list is non-nil terminated

;; a list is circular when some CDRs point to the previous cons cells in the
;; list


;;;; predicates on lists

(comment

 (atom 1)
 (consp '(1 2 3))
 (listp '(1 2 3))
 (listp nil)
 (nlistp 1)
 (null nil)

 ;; a proper list, not circular, not dotted
 (proper-list-p '(1 2 3))

)


;;;; accessing elements of lists

(comment

 (car '(1 2 3))
 (cdr '(1 2 3))

 (caar '((a) b c))
 (cadr '((a) b c))
 (cddr '((a) b c))

 (car-safe 1)
 (cdr-safe 1)

 (setq s1 '(1 2 3))
 (pop s1)

 (nth 1 '(1 2 3))
 (nthcdr 2 '(1 2 3))
 (last '(1 2 3))
 (butlast '(1 2 3))
 (setq s2 '(1 2 3))
 (nbutlast s2)

 (take 2 '(1 2 3))
 (setq s3 '(1 2 3))
 (ntake 2 s3)

 (length '(1 2 3))
 (safe-length 1)

)


;;;; building cons cells and lists

(comment

 (cons 1 nil)
 (cons 1 '(2 3))
 (cons 1 2)

 (list 1 2 3)

 (make-list 5 1)

 ;; the last argument is CDR of the new list
 (append '(1 2) '(3 4) '(5 6))
 (append [1 2] [3 4] [5 6])

 (copy-tree '(1 2 3))

 (flatten-tree '(1 (2 . 3) nil (4 5 (6)) 7))

 (ensure-list 1)

 (number-sequence 0 10 2)

)


;;;; modifying list variables

(comment

 (setq s1 '(1 2 3))

 (push 0 s1)

 (add-to-list 's1 -1)

 (add-to-ordered-list 's1 7 1)
 (add-to-ordered-list 's1 8 3)
 (plist-get (symbol-plist 's1))

)


;;;; modifying existing list structure

;;;; altering the cdr of a list

;; destructive operations should not be applied to quoted lists, only to
;; constructed ones

(comment

 (setq x (list 1 2 3))
 (setcar x 4)

 (setq x1 (list 'a 'b 'c))
 (setq x2 (cons 'z (cdr x1)))

 ;; change CAR of a shared link
 (setcar (cdr x1) 'foo)
 x1
 x2

 ;; change CAR of non-shared link
 (setcar x1 'baz)
 x1
 x2

 (setq x3 (list 1 2 3))
 (setcdr x3 '(4))
 x3

)


;;;; functions that rearrange lists

(comment

 (setq x4 (list 1 2 3))
 ;; the last list is not altered
 (nconc x4 '(4 5))
 x4

)


;;;; using lists as sets

(comment

 (setq x5 '(a b c b a))

 ;; `eq', `equal' or `eql'
 (memq 'b x5)
 (member 'b x5)
 (memql 1 '(1 2 3))

 ;; can modify the list
 (setq x6 '(a b c b a))
 (delq 'b x6)
 (setq x6 '(a b c b a))
 (delete 'b x6)
 (setq x6 '(a b c b a))
 (delete-dups x6)

 ;; returns a new list
 (setq x7 '(a b c b a))
 (remq 'b x7)
 (remove 'b x7)
 (seq-uniq x7)

 (member-ignore-case "fo" '("of" "Fo"))

)


;;;; association lists

;; an association list is a list that contains associations --- mappings from
;; keys to values (cons cells with key in CAR and value in CDR)

;; may be used as a stack

(comment

 (setq a1 '((pine . cones)
            (oak . acorns)
            (maple . seeds)
            (potato (fruits roots))))

 (setq a2 '((rose red)
            (lily white)
            (buttercup yellow)))

 (assoc 'oak a1)
 (assoc 'birch a1)
 (assoc 'lily a2)
 ;; uses `eq', fine for symbol keys and faster
 (assq 'lily a2)
 ;; returns only value
 (alist-get 'lily a2)
 (alist-get 'daisy a2 :nope)
 ;; uses a fn to compare elements/their CARs and given key
 (assoc-default 'lily a2 (lambda (elt key) (eq elt 'rose)))

 (setq a3 '(("Foo" . 1) ("baR" . 2) "oops"))

 (add-to-list 'a3 (cons 3 4))

 (assoc-string "foo" a3 t)
 (assoc-string "oops" a3)


 ;; search by value
 (rassoc 'seeds a1)
 (rassq 'acorns a1)


 (setq a4 (copy-alist a2))

 ;; can modify the alist
 (assq-delete-all 'rose a4)
 (assoc-delete-all 'rose a4)
 (setq a5 (copy-alist a1))
 (rassq-delete-all 'seeds a5)


 (setq colors '((rose . red) (lily . white) (buttercup . yellow)))
 (let-alist colors
   (if (eq .rose 'red)
       .lily))

 (setq colors '((rose . red) (lily (belladonna . yellow) (brindisi . pink))))
 (let-alist colors
   (if (eq .rose 'red)
       .lily.belladonna))

)


;;;; property lists

;;;; property lists and association lists

;;;; property lists outside symbols

;; property list is a list of paired elements; each pair associates a property
;; name (usually a symbol, so functions manipulating it use `eq' by default)
;; with a property value

;; used to store metadata on strings, buffers (text properties), symbols (symbol
;; properties)

;; order of pairs is not significant, since property names must be distinct

(comment

 (setq p1 '(pine cones numbers (1 2 3) color "blue"))
 (setq p2 (symbol-plist 'car))


 (plistp p1)

 (plist-member p1 'color)
 (plist-member p1 'nope)

 (plist-get p2 'pure)
 (plist-get p1 'numbers)

 (plist-put p1 'foo 'bar)

)
