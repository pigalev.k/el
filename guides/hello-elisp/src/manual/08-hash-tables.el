;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; a hash table is a very fast kind of a lookup table

;; lookup is practically constant time; entries are not ordered; structure
;; cannot be shared, like with lists


;;;; creating hash tables

;;;; hash table access

(comment

 (setq h1 (make-hash-table :test #'eq
                           :weakness t
                           :size 100
                           :rehash-size 2
                           :rehash-threshold 0.4))
 (setq h2 #s(hash-table size 10 data (key1 val1 key2 300)))

 (gethash 'key2 h2)
 (puthash 'foo 123 h1)
 (remhash 'key1 h2)
 (clrhash h1)
 (maphash (lambda (key val) (prin1 (format "%s: %s" key val))) h2)

)


;;;; defining hash comparisons

;; values in a hash table are stored and looked up based on hashcodes of their
;; keys

;; to define a new method of key lookup, you need to specify two functions: to
;; compute the hash code from a key (to find a bucket), and a function to
;; compare two keys directly (to see if the key is there); the functions should
;; be consistent --- hashcodes of equal keys should be equal

(comment

 ;; builtin hash functions
 (sxhash-equal 'foo)
 (sxhash-eql 'foo)
 (sxhash-eq 'foo)

 (defun string-hash-ignore-case (s)
   (sxhash-equal (upcase s)))

 (define-hash-table-test 'ignore-case
                         'string-equal-ignore-case
                         'string-hash-ignore-case)

 (setq h3 (make-hash-table :test 'ignore-case))

 (puthash "Foo" 123 h3)
 (gethash "foo" h3)

)


;;;; other hash table functions

(comment

 (setq h4 (make-hash-table))
 (puthash :id 1 h4)
 (puthash :name "Joe" h4)
 (puthash :tags [:foo :bar] h4)

 (hash-table-p h4)
 (setq h5 (copy-hash-table h4))
 (hash-table-count h4)
 (hash-table-test h4)
 (hash-table-weakness h4)
 (hash-table-rehash-size h4)
 (hash-table-rehash-threshold h4)
 (hash-table-size h4)

)
