;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;;;; some terms

;; "the Lisp reader" and "the Lisp printer" are Lisp procedures that convert
;; textual representations of Lisp objects into actual Lisp objects, and vice
;; versa


;;;; `nil' and `t'

;; `nil' and '() are the same object; it is also logical false, and any non-nil
;;  value is logical true, but `t' is preferred; `nil' and `t' always evaluate
;;  to themselves and need not to be quoted

(comment

 ;; predicates

 (booleanp t)
 (booleanp nil)
 (booleanp 1)

)


;;;; evaluation

;; a Lisp expression that you can evaluate is called a "form"; evaluating a form
;; always produces a result, which is a Lisp object

;; when a form is a macro call, it expands into a new form to evaluate


;;;; version information

(comment

 (emacs-version)

 emacs-version
 emacs-major-version
 emacs-minor-version

 emacs-build-time
 emacs-build-number
 emacs-build-system

 emacs-repository-version
 emacs-repository-branch

)
