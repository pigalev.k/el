;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; a Lisp program is a set of expressions (forms); we can control the order of
;; execution of these forms by enclosing them in control structures --- special
;; forms (or macros that delegate to special forms) that control when, whether,
;; or how many times their subforms will be evaluated

;; the simplest (also implicit) order of execution is sequential execution
;; (textual order); explicit control structures allow different orders of
;; execution


;;;; sequencing

;; sequencing can be made explicit when necessary

(comment

 ;; returns the value of the last form

 (progn
   (prin1 "one")
   (prin1 "two")
   (+ 1 2 3))

 ;; returns the value of the first form

 (prog1
     (+ 1 2 3)
   (prin1 "one")
   (prin1 "two"))

 ;; returns the value of the second form

 (prog2
     (prin1 "one")
     (+ 1 2 3)
   (prin1 "two"))

)


;;;; conditionals

(comment

 (if t :true :false)
 (if nil :true :false)
 (if-let ((x 123))
     (* 12 x)
   :nope)
 (if-let ((x nil))
     (* 12 x)
   :nope)

 (when t :ok)
 (when nil :ok)

 (when-let ((x 123))
   (* 12 x))

 (let ((xs '(1 2 3 4)))
   (while-let ((x (car xs)))
     (setq xs (cdr xs))
     (prin1 x)))

 (unless t :ok)
 (unless nil :ok)

 (let ((x t))
   (cond
    ((null x) :false)
    ((eq t x) :true)
    (:else :else)))

  (let ((x nil))
   (cond
    ((null x) :false)
    ((eq t x) :true)
    (:else :else)))

 (let ((x 123))
   (cond
    ((null x) :false)
    ((eq t x) :true)
    (:else :else)))

)


;;;; constructs for combining conditions

(comment

 (not t)
 (not nil)
 (xor t nil)
 (xor t t)
 (xor nil nil)

 ;; short-circuit --- never evaluate non-needed forms (return immediately when
 ;; result is determined)
 (and 1 2)
 (and 1 nil)
 (or 1 nil)
 (or nil nil)

)


;;;; pattern-matching conditional

;; allows pattern-matching programming style; tests a value against a set of
;; patterns (predicate expressions)


;;;; the `pcase' macro

(comment

 (defun test (x)
   (pcase x
     ('(1 . 2) :pair-1-2)
     (`(,_ ,_ ,_) :list-3-elements)
     (:a :keyword-a)
     (1234 :number-1234)
     ("foo" :string-foo)
     ('sym :symbol-sym)

     (`(,_ ,_ ,(pred keywordp) ,_) :list-with-third-keyword)

     ((and (pred numberp)
           (guard (< x 0)))
      :negative-number)
     ((and (pred numberp)
           n
           (and (guard (> n 100))
                (guard (< n 1000))))
      :number-gt-100)
     ((and (pred numberp)
           (guard (> x 1000)))
      :num-gt-1000)
     ((pred numberp) :number)
     ((pred stringp) :string)

     (_ :nothing-matches)))

 ;; exact value patterns
 (test '(1 . 2))
 (test '(1 2 3))
 (test :a)
 (test 1234)
 (test "foo")
 (test 'sym)

 ;; structural patterns
 (test '(1 2 :k 4))

 ;; symbols, preds, guards, logical patterns
 (test 123)
 (test 12345)
 (test -123)
 (test "hello")
 (test 1)
 (test 0)

 ;; catch-all pattern
 (test :nope)


 ;; TODO: app, let, rx, backref, cl-type

)


;;;; extending `pcase'

;; usually rewrite the invoked pattern to use more basic patterns

(comment

 (pcase-defmacro less-than (n)
   "Matches if EXPVAL is a number less than N."
   `(pred (> ,n)))

 (pcase-defmacro integer-less-than (n)
   "Matches if EXPVAL is an integer less than N."
   `(and (pred integerp)
         (less-than ,n)))

 (pcase 123
   ((integer-less-than 1000) :yes)
   (_ :no))

 (pcase 12345
   ((integer-less-than 1000) :yes)
   (_ :no))

)


;;;; backquote-style patterns

(comment

 (pcase '("first" 2)
   (`("first" ,_) :list-with-first))

 (pcase '(1 . 2)
   (`(,_ . 2) :pair-ending-with-2))

 (pcase [1 2 3]
   (`[1 ,_ ,_] :vector-starting-with-1))

 (defun evaluate (form env)
   (pcase form
     (`(add ,x ,y)       (+ (evaluate x env)
                            (evaluate y env)))
     (`(call ,fun ,arg)  (funcall (evaluate fun env)
                                  (evaluate arg env)))
     (`(fn ,arg ,body)   (lambda (val)
                           (evaluate body (cons (cons arg val)
                                                env))))
     ((pred numberp)     form)
     ((pred symbolp)     (cdr (assq form env)))
     (_                  (error "Syntax error: %S" form))))

 (evaluate '(add 1 2) nil)
 (evaluate '(add x y) '((x . 1) (y . 2)))
 (evaluate '(call (fn x (add 1 x)) 2) nil)
 (evaluate '(sub 1 2) nil)

)


;;;; destructuring with `pcase' patterns

;; destructuring is extraction of multiple values stored in an object; `pcase'
;; allows destructuring binding with `pcase-let'; another destructuring facility
;; is `seq-let' (see Sequence Functions)

(comment

 (pcase '(add 1 2)
   (`(add ,x ,y) (message "Contains %S and %S" x y)))

 (pcase-let ((`(add ,x ,y) '(add 1 2)))
   (message "Contains %S and %S" x y))

 (pcase-let* ((`(,major ,minor)
	           (split-string "image/png" "/")))
   minor)

 (pcase-dolist
     (`(,a ,_) '((1 2) (2 3) (3 4)))
   (prin1 a))

 (pcase-setq `(,a ,b ,_) '(1 2 3))
 a
 b

 (setq fun
       (pcase-lambda (`(,key . ,val))
         (vector key (* val 10))))
 (funcall fun '(foo . 2))

)


;;;; iteration

(comment

 (setq num 0)

 (while (< num 4)
   (princ (format "Iteration %d." num))
   (setq num (1+ num)))

 (while (not (looking-at "^$"))
   (forward-line 1))

 (while (progn
          (forward-line 1)
          (not (looking-at "^$"))))

 (let (result)
   (dolist (a '(1 2 3) result)
     (setq result (cons a result))))

 (dotimes (i 10)
   (princ i))

)


;;;; generators

;; a generator is a function that produces a potentially infinite stream of
;; values; each time the function produces a value, it suspends itself and waits
;; for a caller to request the next value

(comment

 (require 'generator)

 (iter-defun my-iter (x)
   (iter-yield (1+ (iter-yield (1+ x))))
   ;; Return normally
   -1)

 (let* ((iter (my-iter 5))
        (iter2 (my-iter 0)))
   ;; Prints 6
   (print (iter-next iter))
   ;; Prints 9
   (print (iter-next iter 8))
   ;; Prints 1; iter and iter2 have distinct states
   (print (iter-next iter2 nil))

   ;; We expect the iter sequence to end now
   (condition-case x
       (iter-next iter)
     (iter-end-of-sequence
      ;; Prints -1, which my-iter returned normally
      (print (cdr x)))))

 ;; not quite clear what happens here
 (setq i1 (my-iter 12))
 (iter-next i1 9)
 (iter-do (x i1)
   (princ x))
 (iter-close i1)

 ;; TODO: should return here and squint harder looking for potential uses

 )


;;;; nonlocal exits

;; a nonlocal exit is a transfer of control from one point in a program to
;; another remote point; they can occur as a result of errors, or can be used
;; explicitly

;; nonlocal exits unbind all variable bindings made by the constructs being
;; exited

;; `throw' restores the buffer and position saved by `save-excursion', and
;; narrowing status saved by `save-restriction'; it also runs any cleanups
;; established with the `unwind-protect' when it exits this form

;; an innermost `catch' with the same tag as in `throw' is selected; if
;; something is thrown but no `catch' is specified, an error is signaled

;; `catch' returns the value given by `throw', or the normal value of its body
;; expression if nothing was thrown

;; note: all this has dynamic scope and infinite extent

;;;; explicit nonlocal exits: `catch' and `throw'

;;;; examples of `catch' and `throw'

(comment

 (defun foo-inner ()
   (throw 'foo :thrown))

 (defun foo-outer ()
   (catch 'foo
     (foo-inner)))

 (foo-outer)

 ;; signals an error
 (throw 'bar 1)

)


;;;; errors

;; when a form cannot, for some reason, be evaluated, an error is signaled

;; default reaction to an error is to print error message and terminate
;; execution of the current command

;; `unwind-protect' is used to establish cleanup expressions to be evaluated in
;; case of error

;; `condition-case' is used to establish error handlers to recover control in
;; case of error (e.g., continue execution)

;; to report problems without terminating the execution of the current command,
;; issue a warning instead


;;;; how to signal an error

;; signaling an error means beginning error processing; it normally aborts all
;; or part of the running program and returns to a point that is set up to
;; handle the error

;; most errors are signaled automatically from Lisp primitives (e.g. when trying
;; to get a CAR of an integer); it can be done from Lisp with `error' and
;; `signal'

;; every error specifies an error message; it should state what is wrong, not
;; how it must be; convention --- start an error message with a capital letter,
;; end without punctuation

(comment

 (error "Frob does not exist: `%s'" "oops")

 (define-error 'foo-error "Can't bear this anymore")
 (signal 'foo-error nil)
 (signal 'foo-error '(1 2 3))

 (signal 'no-such-error '("My unknown error condition"))

 ;; meant to report errors of the user, not in the code (think 400 vs 500 HTTP
 ;; response statuses); do not enter the debugger
 (user-error "Something weird happened: look %s" "up")

)


;;;; how Emacs processes errors

;; when an error is signaled, `signal' searches for an active handler for the
;; error; a handler is a sequence of expressions to be executed in case of an
;; error

;; if the error has an applicable handler, it is executed, and control resumes
;; following the handler; the handler is executed in the environment of the
;; `condition-case' that established it; all functions called within that
;; `condition-case' have already been exited, the handler cannot return to them

;; if there is no applicable handler, the current command is terminated and
;; control is returned to the editor command loop (an implicit handler for all
;; kinds of errors); the command loop's handler uses the error symbol and
;; associated data to print an error message

;; an error that has no explicit handler may call the Lisp debugger (that runs
;; in environment of the error); handled errors usually not

(comment

 ;; the command loop error handler
 command-error-function

 debug-on-error
 debug-on-signal

)


;;;; writing code to handle errors

(comment

 (condition-case e
     ;; protected form
     (progn
       ;; (/ 1 0)
       (error "Oops: %s" "meh"))
   ;; handlers
   (arith-error (list :arith-error-handled e))
   (error (list :error-handled e)))

 ;; rethrow
 (condition-case e
     (error "Oops: %s" "meh")
   (error (signal (car e) (cdr e))))

 ;; do not suppress the call to the debugger
 (condition-case nil
     (/ 1 0)
   ((debug error) :debugged))

 ;; error type message
 (condition-case e
     (/ 1 0)
   (arith-error (message "%s" (error-message-string e))))

 (condition-case s
     (+ 1 2 3)
   (:success (format "Success: %d" s)))

 (let ((debug-on-error nil))
   (condition-case-unless-debug nil
       (/ 1 0)
     (error :error-handled)))

 (ignore-error arith-error
   (/ 1 0))

 (ignore-errors
   (error "Oops"))

 (let ((debug-on-error nil))
   (with-demoted-errors "Error: %s"
     (/ 1 0)))

)


;;;; error symbols and condition names

;; when an error is signaled, `signal' is given an error symbol to specify the
;; kind of error; each error have one corresponding symbol; errors grouped into
;; a hierarchy of error conditions, identified by condition names; each error
;; symbol is also a condition name; so each error has one or more condition
;; names, starting with it's symbol name and including names of its parents'
;; symbols, transitively

(comment

 ;; the message is printed when the error is signaled but not handled (and
 ;; debugger is not called)
 (define-error 'foo-error "Something happened")
 (define-error 'bar-error "Too many shots tonight" 'foo-error)

 (let ((debug-on-error nil))
   (signal 'bar-error '(1 3 5)))

 ;; a hierarchy of errors; allows to handle errors at different levels of
 ;; abstraction
 (symbol-plist 'bar-error)

)


;;;; cleaning up from nonlocal exits

(comment

 (let ((buffer (get-buffer-create " *temp")))
   (with-current-buffer buffer
     (unwind-protect
         ;; protected form; can signal an error, throw out or complete normally
         (princ "processing")
       ;; cleanup forms; not protected themselves
       (kill-buffer buffer)
       (princ "done"))))

)
