;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; the byte compiler translates functions written in Lisp into a special
;; representation --- byte-code, that can be executed more efficiently

;; the compiler replaces Lisp function definitions with byte-code; when a
;; byte-code function is called, its definition is evaluated by the byte-code
;; interpreter

;; byte-code is more portable than machine code, but not as fast


;;;; performance of byte-compiled code

(comment

 (defun silly-loop (n)
   "Return the time, in seconds, to run N iterations of a loop."
   (let ((t1 (float-time)))
     (while (> (setq n (1- n)) 0))
     (- (float-time) t1)))

 (silly-loop 50000000)

 (byte-compile 'silly-loop)
 (silly-loop 50000000)

)


;;;; byte-compilation functions

(comment

 (defun foo (x) (* x x))
 (foo 5)
 (symbol-function 'foo)

 ;; replaces the function value of 'foo, does not handle function indirection
 (byte-compile 'foo)
 (foo 5)
 (symbol-function 'foo)


 (byte-compile-file "common.el")
 (byte-recompile-directory "." 0)

)


;;;; documentation strings and compilation

;; some obscure and probably mostly irrelevant problems; reload after
;; recompilation to avoid

(comment

 byte-compile-dynamic-docstrings

)


;;;; dynamic loading of individual functions

;; the dynamic function loading (lazy loading) is something like autoloading,
;; but for compiled functions, disabled by default (on this machine at least);
;; deprecated since 27.1

;; some obscure and probably mostly irrelevant problems; reload after
;; recompilation to avoid

(comment

 byte-compile-dynamic

 (fetch-bytecode 'foo)

)


;;;; evaluation during compilation

(comment

 ;; evaluated when run and when compiled
 (eval-and-compile
   (message "when compiled or run (with or without compilation)"))

 (eval-when-compile
   (message "when compiled or run without compilation"))

)


;;;; compiler errors

;; see "*Compile-Log*" for errors and warnings, " *Compiler Input" for clues on
;; syntax error locations

(comment

 (with-suppressed-warnings ((obsolete foo))
   (message "with suppressed warnings"))

 (with-no-warnings
   (message "with completely suppressed warnings"))

 byte-compile-warnings
 byte-compile-error-on-warn

)


;;;; byte-code function objects

;; byte-compiled function is a special data type; when such a function is
;; called, it is executed by the byte-code interpreter

;; internally it is like a vector, with a leading hash in the printed
;; representation; elements (min 4): descriptor of the arguments, byte-code
;; (string), constants (vector), stacksize, docstring, interactive spec

(comment

 ;; do not do it by hand, Emacs may crash on call if args are inconsistent
 ;; (make-byte-code ...)

)


;;;; disassembled byte-code

;; the byte-code interpreter is a simple stack machine; values are pushed onto a
;; stack and popped to be used in calculations whose results are pushed back
;; onto the stack; when a byte-code function returns, it pops the value off the
;; stack and returns it as the value of the function

;; byte-code functions also can access, bind and set Lisp variables by
;; transferring values between variables and the stack

(comment

 (disassemble (lambda (x) (* x x)))

 (defun factorial (integer)
   "Compute factorial of an integer."
   (if (= 1 integer) 1
     (* integer (factorial (1- integer)))))

 (disassemble 'factorial)

)


;;;; compilation of Lisp to native code

;; natively-compiled code is executed directly by the machine's hardware; this
;; is faster than byte-code (let alone source code) interpretation, but less
;; portable

;; it seems that natively-compiled Lisp libraries become dynamic shared
;; libraries used by Emacs


;;;; native-compilation functions

;;;; native-compilation variables

(comment

 (native-comp-available-p)


 (setq sq (native-compile (lambda (x) (* x x))))
 (funcall sq 5)

 (native-compile-async "common.el")


 native-comp-speed
 native-comp-debug
 native-comp-verbose

 native-comp-async-jobs-number
 native-comp-async-report-warnings-errors
 native-comp-async-query-on-exit
 native-comp-jit-compilation
 native-comp-enable-subr-trampolines

)
