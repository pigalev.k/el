;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; loading a file of Lisp code means bringing its contents into the Lisp
;; environment in the form of Lisp objects; Emacs finds and opens the file,
;; reads the text, evaluates each form, and closes the file (a Lisp library)

;; the file must contain Lisp expressions, either as source code or as
;; byte-compiled code; each form in the file is a top-level form

;; Emacs can also load compiled dynamic modules --- shared libraries that
;; provide additional functionality for use in Emacs Lisp programs (just like
;; packages written in Emacs Lisp); when a dynamic module is loaded, Emacs calls
;; an initialization function which exposes the additional functions and
;; variables to Lisp programs


;;;; how programs do loading

;; - `autoload' creates a placeholder for a function defined in a file that is
;;   loaded on demand

;; - `require' loads a file if it isn't already loaded

;; - `load' actually loads source files and is used by the previous two

(comment

 ;; looks for `filename.elc', if found, looks for `filename.eln' and loads it;
 ;; else loads `filename.elc'

 ;; else looks for `filename.el' and loads it; then looks for `filename.ext'
 ;; (dynamic module, ".ext" is system-dependent; ".so" in Linux)

 ;; else looks for `filename' and loads it

 ;; may search for a compressed version of the file or use exact file names
 ;; without suffixes if configured to do so

 ;; if given a relative path, will search in the `load-path' and use the first
 ;; file found; sets `load-in-progress', `load-file-name' and
 ;; `load-true-file-name' during loading

 ;; when loading an uncompiled file, tries to expand any macros in the file
 ;; (eager macro expansion; a performance optimization); loading a compiled file
 ;; does not cause macro expansion --- it should already have happened during
 ;; compilation

 ;; unhandled errors terminate loading

 (load (expand-file-name "common.el"))

 ;; like `load', but can read its argument interactively

 (load-library "seq")

 ;; does not use `load-path', looks in the current directory; does not append
 ;; suffixes; can read its argument interactively

 (load-file "common.el")


 load-in-progress
 load-file-name
 load-true-file-name
 load-read-function

)


;;;; load suffixes

(comment

 load-suffixes
 load-file-rep-suffixes

 (get-load-suffixes)

 load-prefer-newer

)


;;;; library search

(comment

 ;; nil element means the current directory
 load-path
 lisp-directory

 ;; by default there is the Emacs' own lisp directory (`lisp-directory') and
 ;; host-specific `$version/site-lisp' and `site-lisp' directories

 ;; obeys `EMACSLOADPATH' env var if set, syntax is the same as in `PATH'; the
 ;; default load path is appended before leading or after trailing separator
 ;; (colon), if present

 ;; for each dir in `load-path', `subdirs.el' is loaded if present

 ;; any extra load dirs specified with -L command line option are added


 (locate-library "seq")
 (list-load-path-shadows t)

 native-comp-eln-load-path
 comp-native-version-dir

)


;;;; loading non-ASCII characters

;; text can be represented as unibyte or multibyte strings; this is usually
;; handled automatically just fine


;;;; autoload

;; autoloading allows to register the existence of a function or macro but defer
;; loading of the file that defines it; the first call to (and maybe looking up
;; the documentation of, also completion of variable and function names) the
;; function/macro loads the proper library and invokes the real definition as if
;; it had been loaded all along

;; two ways to set up autoload: by calling `autoload', and by writing a magic
;; comment in the source before the real definition, that will guide
;; `loaddefs-generate', which constructs calls to `autoload' and arranges their
;; execution


;; autoload magic comment (autoload cookie), just before the definition

;;;###autoload
(defun foo (x) (* x x))

;; corresponding autoload call will be added to file that will be loaded on
;; startup (`loaddefs.el' by default)

(comment

 ;; will not overwrite existing function object of 'foo, if any
 (autoload 'foo "foo.el" "A foo function." t)
 (symbol-function 'foo)

 (autoloadp (symbol-function 'foo))
 (autoload-do-load (symbol-function 'foo))

 lisp-mode-autoload-regexp


 ;; generates loaddefs files for all .el files in the dir
 (loaddefs-generate "." "manual-loaddefs.el")

)


;;;; autoload by prefix

;; some details for autoloading on completion

;;;; when to use autoload

;; - for interactive entry points to a library

;; don't autoload variables, user options; don't use autoload comment to silence
;; a warning in another file (maybe autoload statement)


;;;; repeated loading

;; a file can be loaded more than once in a session; nonidempotent forms can
;; cause trouble


;;;; features

;; `provide' and `require' also can load files automatically; they work in terms
;; of features --- a feature is loaded the first time another program asks for
;; it by name

;; a feature name is a symbol that stands for a collection of functions,
;; variables, etc; the file that defines them should `provide' the feature;
;; program that uses them can ensure they are defined by `require'ing the
;; feature; this loads the file of definitions if it hasn't been loaded already

(comment

 features

 ;; adds 'foo-lib to `features', executes any `eval-after-load'ed code
 (provide 'foo-lib)

 ;; loads a file (based on the feature name by default + .el/.elc) when the
 ;; feature is not present, signals an error if the feature is not provided in
 ;; the file; if it is already present, does nothing
 (require 'foo-lib)

 (featurep 'foo-lib)


 ;; combines requiring and configuring the feature (similarly to load-time
 ;; hooks); see its manual for more options
 (use-package json-mode
   :init (message "initialized")
   :config (message "configured"))

)


;;;; which file defined a certain symbol

(comment

 load-history

 (symbol-file 'foo nil)

 (plist-get (symbol-plist 'foo) 'function-history)

)


;;;; unloading

(comment

 (unload-feature 'json-mode)

 unload-feature-special-hooks

)


;;;; hooks for loading

(comment

 after-load-functions

 ;; see *Messages*
 (with-eval-after-load 'json-mode
   (message "json mode loaded"))

 (require 'json-mode)

 ;; normally, this should not be used --- just use `require' first

)


;;;; Emacs dynamic modules

;; a dynamic Emacs module is a shared library that provides additional
;; functionality for use in Lisp programs, just like a package written in Lisp

;; functions that load Lisp packages can also load dynamic modules, recognizing
;; them by the filename extension

(comment

 module-file-suffix

 ;; further details omitted here for now

)
