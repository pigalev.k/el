(load-file "../common.el")


;; a buffer if a Lisp object containing text to be edited --- e.g., contents
;; of a file that is being visited

;; any buffer may or may not be displayed in any windows; most editing
;; commands act on the contents of the current buffer


;;;; buffer basics

;; a buffer is like a mutable string; insertions and deletions may occur in
;; any part of the buffer

;; directly (without use of a function) accessible buffer-specific information
;; is stored in buffer-local variable bindings; values of these variables are
;; effective only in a particular buffer

(comment

 (bufferp (current-buffer))

)


;;;; the current buffer

;; several buffers may exist at once, but only one buffer is designated the
;; current buffer at any time

(comment

 (current-buffer)

 ;; when an editing command returns to the editor command loop, it is
 ;; automatically called on the buffer shown in the selected window
 (set-buffer (current-buffer))

 ;; beware of switching buffers during binding forms, such as `let'
 (save-current-buffer
   (set-buffer (find-file-noselect "out.txt"))
   (insert "hello! ")
   (basic-save-buffer))

 (with-current-buffer (find-file-noselect "out.txt")
   (insert "hello again! ")
   (basic-save-buffer))

 (with-temp-buffer
   (insert "a text")
   (buffer-string))

)


;;;; buffer names

;; each buffer has a unique name, a string; many functions accept buffer or
;; buffer name

(comment

 (buffer-name (current-buffer))

 (get-buffer "out.txt")

 (with-temp-buffer
   (rename-buffer " temporary buffer")
   (buffer-name))

 (generate-new-buffer-name "out.txt")

)


;;;; buffer file name

(comment

 buffer-file-name
 buffer-file-truename
 buffer-file-number

 (buffer-file-name (get-file-buffer "out.txt"))

 ;; may return any buffer visiting the file, possibly with a different name
 (find-buffer-visiting "out.txt")

 (with-temp-buffer
   (set-visited-file-name "out1.txt")
   (buffer-file-name))

 list-buffers-directory
 (let ((list-buffers-directory "~~none~~"))
   (list-buffers))

)


;;;; buffer modification

;; a modified flag is kept for each buffer, indicating whether the buffer's
;; text was changed; it is set on edits and cleared on save

(comment

 (buffer-modified-p)

 (set-buffer-modified-p t)

 (restore-buffer-modified-p t)

 (not-modified)

 (buffer-modified-tick)
 (buffer-chars-modified-tick)

 (with-silent-modifications
   (insert "modified silently"))

)


;;;; buffer modification time

(comment

 (verify-visited-file-modtime)

 (clear-visited-file-modtime)

 (visited-file-modtime)

 (set-visited-file-modtime)

 (ask-user-about-supersession-threat "out.txt")

)


;;;; read-only buffers

(comment

 buffer-read-only
 inhibit-read-only

 (read-only-mode 'toggle)
 (barf-if-buffer-read-only)

)
