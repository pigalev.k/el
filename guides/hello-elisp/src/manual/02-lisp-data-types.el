;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;;;; Lisp data types

;; a Lisp object is a piece of data used and manipulated by Lisp programs; a
;; data type is a set of possible objects

;; every object belongs to at least one type, but maybe more

;; a few fundamental object types (primitive types) are built into Emacs; from
;; them all other types are constructed: integer, float, cons, symbol, string,
;; vector, hash-table, subr, byte-code function, and record, plus several
;; special types, such as buffer, that are related to editing

;; each primitive type has a corresponding Lisp function that checks whether an
;; object is a member of that type

;; objects have types, that are implicit in the object itself, but type
;; declarations for variables do not exist in Emacs Lisp; a variable can have
;; any type of value (with some exceptions like variables with restricted
;; values)


;;;; printed representation and read syntax

;; the printed representation of an object is the format of the output generated
;; by the Lisp printer (function `prin1') for that object; every data type has a
;; unique printed representation

;; the read syntax of an object is the format of the input accepted by the Lisp
;; reader (the function `read') for that object; object can have more than one
;; syntax

;; in most cases, the object's printed representation and the read syntax are
;; the same; but some types have no read syntax, they are printed in hash
;; notation (like #<object description>), that cannot be read

;; in other languages, an expression is text and has no other form; in Lisp, an
;; expression is primarily a Lisp object and only secondarily the text (the
;; object's read syntax)

;; when an expression is evaluated, the Lisp interpreter first reads the textual
;; representation of it, producing a Lisp object, and then evaluates that
;; object; reading and evaluation are separate activities --- reading returns
;; the Lisp object represented by the text that is read, and this object may or
;; may not be evaluated later


;;;; special read syntax (hash notations)

(comment

 ;; an "invalid read syntax" error
 (read "#<>")

 ;; interned symbol with empty string name
 (read "##")

 ;; a function object
 (defun foo () nil)
 (read "#'foo")

 ;; uninterned symbol `foo'; normally reader interns all symbols
 (read "#:foo")

 ;; printed representation for self-referent objects
 (let ((a (list 1)))
   (setcdr a a))

 ;; read syntax for circular and self-referent objects
 (read "(#1=a b #1#)")
 (read "#1=(a #1#)")

 ;; hexadecimal number
 (read "#xa")

 ;; binary number
 (read "#b100")

 ;; string text properties
 (read "#(\"foo bar\" 0 3 (face bold) 3 4 nil 4 7 (face italic))")

 ;; char tables: #^[...]; sub-char tables: #^^[...]

 ;; hash tables
 (read "#s(hash-table)")

 ;; characters
 (read "?C")

 ;; the current file name in byte-compiled files
 (read "#$")

 ;; skip the next N characters (used in byte-compiled files)
 (read "#@4")

 ;; indicates that the following form isn't readable by the reader; for printing
 ;; only: #f


)


;;;; comments

;; comments start at semicolon (unless within a string or character constant)
;; and continue to the end of line; reader discards comments



;;;; programming types

;; there are two general categories of types in Emacs Lisp: those having to do
;; with Lisp programming, and those having to do with editing


;; integer type

(comment

 ;; integers may be small (fixnums, minimum 30 bits) or large (bignums,
 ;; arbitrary precision); operations that overflow a fixnum will return a bignum
 ;; instead

 ;; numbers are compared with `eql' or `='; fixnums can also be compared with
 ;; `eq' (more on equality in the corresponding chapter)

 (= 1 1)
 (= 1 1.0)

 (eq 1 1.0)
 (eql 1 1.0)

 -1
 1
 1.
 +1


 ;; predicates

 (fixnump 1)
 (fixnump 1000000000000000000)
 (bignump 10000000000000000000)

 (numberp 1)
 (integerp 1)
 (wholenump 1.0)
 (natnump 1)
 (zerop 0)

 (integer-or-marker-p 1)
 (number-or-marker-p 1)

)


;; floating-point type

(comment

 ;; floating-point numbers are the computer equivalent of scientific notation
 ;; --- a fraction together with a power of ten; C data type "double" is used,
 ;; so precision is machine-specific

 1500.0
 +15e2
 15.0e+2
 +1500000e-3
 .15e4


 ;; predicates

 (numberp 1.0)
 (floatp 1.0)
 (zerop 0.0)

 (number-or-marker-p 1.0)

)


;; character type

(comment

 ;; a character is an integer (a character code).

 ;; characters in strings and buffers are currently limited to the range of 0 to
 ;; 4194303 --- 22 bits; codes 0-127 are ASCII codes, the rest are non-ASCII

 ;; characters that represent keyboard input have a much wider range, to encode
 ;; modifier keys such as Control, Meta and Shift


 ;; basic char syntax

 ?Q
 ?q

 ;; have special syntactic meaning, escaped
 ?\(

 ;; escape sequences
 ?\n
 ?\r
 ?\s
 ?\t
 ?\\

 ;; escaping ordinary characters is harmless
 ?\+


 ;; general escape syntax

 ;; by Unicode name
 ?\N{LATIN SMALL LETTER A WITH GRAVE}

 ;; by Unicode value
 ?\N{U+E0}
 ?\u00e0
 ?\U000000E0
 ?à

 ;; by hexadecimal character code
 ?\x41

 ;; by octal character code
 ?\101


 ;; active modifiers are represented as bits 2**22 (alt), 2**23 (super), 2**24
 ;; (hyper), 2**25 (shift), 2**26 (control), 2**27 (meta)

 ;; control-character syntax
 ?\^I
 ?\C-I

 ;; meta-character syntax
 ?\M-A
 ?\M-\C-b
 ?\M-\002

 ;; other character modifier bits (shift, hyper, super, alt)
 ?\S-a
 ?\H-a
 ?\s-a
 ?\A-a
 ?\H-\M-\A-x


 ;; predicates

 (characterp ?A)
 (char-or-string-p ?A)

)


;; symbol type

(comment

 ;; a symbol is an object with a name; the symbol name serves as the printed
 ;; representation of the symbol; in ordinary use (with single obarray) a
 ;; symbol's name is unique

 'foo

 ;; a symbol can be used as a variable, as a function name, or to hold a
 ;; property list, or only to be distinct from all other Lisp objects; it can be
 ;; used in any of these ways independently

 ;; a symbol whose name starts with a colon is called a keyword symbol and
 ;; automatically act as a constant

 :a
 :foo
 :thank-ya

 ;; any characters allowed in the symbol name, as long as it does not look like
 ;; a number (and then adding backslash at the beginning will force it's
 ;; interpretation as a symbol)

 'foo!123<>{}@$%?&^+-:
 '\(*\ 1\ 2\)
 '\123foo321


 ;; predicates

 (symbolp 'foo)
 (keywordp :foo)
 (custom-variable-p 'compilation-disable-input)

)


;; sequence type

(comment

 ;; a sequence is an object that represents an ordered set of elements; there
 ;; are two kinds of sequence --- lists and arrays

 ;; a list can hold elements of any type, and its elements can be added or
 ;; removed

 ;; an array is a fixed-length sequence; they are further subdivided into
 ;; strings, vectors, char-tables and bool-vectors
 ;; - vectors can hold elements of any type
 ;; - char-tables are like vectors except that they are indexed by any valid
 ;;   character code
 ;; - strings hold characters (that can have text properties)
 ;; - bool-vectors hold t or nil

 ;; certain functions (sequence functions) accept any type of sequence

 (length '(1 2 3))
 (length [1 2 3])

 ;; reading the sequence twice will produce two identical sequences (except for
 ;; empty list, that always stands for the same object, `nil')


 ;; predicates

 (sequencep '())

)


;; cons cell and list types

(comment

 ;; a cons cell is an object that consists of two slots, called the CAR slot and
 ;; the CDR slot; each slot can hold any Lisp object

 ;; a list is a series of cons cells, linked together so that the CDR slot of
 ;; each cons cell holds either the next cons cell or the empty list (symbol
 ;; `nil'); any structure made out of cons cells is called a "list structure";
 ;; objects that are not cons cells are called "atoms".

 ;; the read syntax and printed representation for lists are identical: left
 ;; parenthesis, an arbitrary number of elements, and a right parenthesis

 '()
 '(1 2 3)
 '("A" b c)
 '(a (b (c d)) e)

 ;; `cons' is the cons cell constructor, `car' and `cdr' are accessors

 (setq l (cons 1 (cons 2 '())))
 (car l)
 (cdr l)
 (cdr (cdr l))
 (cdr (cdr (cdr l)))

 (car nil)
 (cdr nil)


 ;; dotted pair notation is a general syntax for cons cells that represents CAR
 ;; and CDR explicitly; CDR does not have to be a list

 (setq dp '(1 . 2))
 (car dp)
 (cdr dp)

 ;; dotted pair and list notations can be combined freely
 '(1 2 3 4 . 5)

 ;; ordinary list read in dotted pair notation
 '(1 . (2 . (3 . nil)))

 ;; when printing a list, the dotted pair notation is only used if the CDR of a
 ;; cons cell is not a list
 '(1 . (2 . 3))


;; predicates

 (sequencep '())

 (atom '())
 (atom 12)
 (atom '(1 2 3))

 (listp '())
 (listp '(1 2 3))

 (consp '())
 (consp '(1 2 3))

 (nlistp 1)
 (null '())
 (proper-list-p '(1 2 3))

)


;; association list type

(comment

 ;; an association list (alist) is a specially-constructed list whose elements
 ;; are cons cells; in each element, the CAR is considered a key, and the CDR
 ;; (or CAR of the CDR) is considered an associated value

 (setq alist-of-colors '((rose . red) (lily . white) (buttercup . yellow)))

)


;; array type

(comment

 ;; an array is composed of an arbitrary (but fixed, up to largest fixnum)
 ;; number of slots for holding or referring to other Lisp objects, arranged in
 ;; a contiguous block of memory; the element access is constant time (instead
 ;; of linear time for a list)

 ;; arrays use zero-origin indexing and are one-dimensional (although arrays can
 ;; be nested if the array type allows it)


 ;; predicates

 (arrayp [])

)


;; string type

(comment

 ;; a string is an array of characters; strings are constants --- their
 ;; evaluation return the same string


 ;; syntax for strings: a double-quote, an arbitrary number of characters, and
 ;; another double-quote

 "a string"

 ;; to include a double-quote in a string, escape it with a backslash; same with
 ;; a backslash itself

 "a \"quoted\" \\word"

 ;; newlines are part of the string, but escaped newlines are ignored

 "It is useful to include newlines
in documentation strings,
but the newline is \
ignored if escaped."


 ;; non-ASCII characters in strings

 ;; there are two text representations for non-ASCII characters in strings:
 ;; multibyte (storing human-readable text, values from 0 to 4194303) and
 ;; unibyte (storing raw bytes, values from 0 to 255); in both cases, characters
 ;; above 127 are non-ASCII

 ;; non-ASCII characters can be written literally or as escape sequences (see
 ;; Character type); non-ASCII characters in strings read from multibyte source
 ;; (multibyte buffer of string, file visited as multibyte) will be read as
 ;; multibyte characters, and likewise for unibyte sources

 ;; terminate hexadecimal and octal sequences with backslash and space when
 ;; needed
 ?\xe0
 "?\xe0a"
 "?\xe0\ a"


 ;; nonprinting characters in strings

 ;; the same escape sequences as in character literals can be used; the only
 ;; control characters in a string are the ASCII control characters, and their
 ;; case is not distinguished
 "\t abc\C-a"

 ;; strings cannot hold meta characters, but there is a convention to represent
 ;; meta versions of ASCII characters in a string: set the 2**7 bit of the
 ;; character; if then the string is used as a key sequence, it will be
 ;; recognized as meta character
 "\C-\M-a"

 (lookup-key emacs-lisp-mode-map "\M-q")

 ;; strings cannot hold characters that have the hyper, super, or alt modifiers


 ;; text properties in strings

 ;; a string can hold properties for the characters it contains, in addition to
 ;; the characters themselves; strings with text properties use a special read
 ;; and print syntax:

 ;; first three characters have a 'face property with value 'bold, and the last
 ;; three have a 'face property with value 'italic; the fourth character has no
 ;; text properties, so its property list is `nil'; it is not necessary to
 ;; mention ranges with `nil' as the property list, as it is the default for
 ;; characters that are not mentioned to have no properties
 #("foo bar" 0 3 (face bold) 3 4 nil 4 7 (face italic))


 ;; predicates

 (sequencep "foo")
 (arrayp "foo")
 (stringp "foo")
 (char-or-string-p "foo")
 (string-or-null-p "foo")

)


;; vector type

(comment

 ;; a vector is a one-dimensional array of elements of any type

 ;; the read syntax and the printed representation of a vector consists of a
 ;; left square bracket, the elements, and a right square bracket; vectors are
 ;; considered constants for evaluation

 [1 "two" (three) :four]

 ;; predicates

 (sequencep [])
 (arrayp [])
 (vectorp [])

)


;; char-table type

(comment

 ;; a char-table is a one-dimensional array of elements of any type, indexed by
 ;; character codes; char-tables have certain extra features that make them
 ;; useful for associating information to character codes (e.g., a parent to
 ;; inherit from, a default value, some extra slots for special purposes)

 ;; char-table can also specify a single value for a whole character set

 ;; uses of char-tables:
 ;; - case tables
 ;; - character category tables
 ;; - display tables

 (setq ct (make-char-table 'foo-table))
 (setq ct1 (standard-case-table))
 (setq st (standard-syntax-table))

 ;; predicates

 (sequencep ct)
 (arrayp ct)
 (char-table-p ct)

 (case-table-p ct1)
 (syntax-table-p st)

)


;; bool-vector type

(comment

 ;; a bool-vector is a one-dimensional array whose elements must be `t' or `nil'

 ;; the printed representation of a bool-vector is like a string, but begins
 ;; with #& followed by the length

 (make-bool-vector 3 t) ; 111
 (make-bool-vector 3 nil) ; 0

 ;; if the length is not a multiple of 8, the printed representation shows extra
 ;; elements, but they make no difference

 ;; only the first 3 bits are used
 (equal #&3"\377" #&3"\007")


 ;; predicates

 (setq bv (make-bool-vector 5 t))
 (sequencep bv)
 (arrayp bv)
 (bool-vector-p bv)

)


;;;; hash table type

(comment

 ;; a hash table is a very fast kind of lookup table that maps keys to values

 ;; the printed representation of a hash table specifies its properties and
 ;; contents:

 (setq ht (make-hash-table))


 ;; predicates

 (hash-table-p ht)

)


;; function type

(comment

 ;; function is executable code; function is also a Lisp object

 ;; non-compiled function is a lambda expression --- a list whose first element
 ;; is the symbol 'lambda; it has no intrinsic name (also called an anonymous
 ;; function)

 ;; a named function is just a symbol with a valid function in its function cell

 ;; functions usually called when their names are written in expressions, but we
 ;; can construct or obtain a function object at runtime and then call it using
 ;; primitive functions `funcall' and `apply'

 (setq f (lambda (x) (* x x)))
 (setq c (lambda (x) (interactive) (* x x)))

 ;; predicates

 (functionp f)
 (commandp c)

)


;; macro type

;; a Lisp macro is a user-defined construct that extends the Lisp language; it
;; is represented as an object much like a function, but with different
;; argument-passing semantics --- a list whose first element is the symbol
;; 'macro and whose CDR is a Lisp function object, including the 'lambda symbol

;; macros are usually defined with `defmacro', but any list that begins with
;; 'macro will do

;; note: Lisp macros and keyboard macros are different things


;; primitive function type

(comment

 ;; a primitive function is a function callable from Lisp but written in C; also
 ;; called "subrs" (subroutines) or "built-in functions"

 ;; most primitive functions evaluate all their arguments when they are called;
 ;; a primitive function that does not evaluate all its arguments is called a
 ;; special form

 ;; it does not matter to the caller whether the function is primitive; but
 ;; redefining primitive function may lead to unanticipated effects (calls from
 ;; Lisp will use the new definition, but calls from C code may use old one)

 ;; primitive functions have no read syntax and print in hash notation with the
 ;; function name

 (symbol-function 'car)


 ;; predicates

 (subrp (symbol-function 'car))

)


;; byte-code function type

;; byte-code function objects are produced by byte-compiling Lisp code;
;; internally, a byte-code function is much like a vector, but the evaluator
;; handles this data type specially when it appears in a function call

;; the printed representation and read syntax for a byte-code function object
;; is like that for a vector, with # before the opening bracket


;; record type

(comment

 ;; a record is much like a vector, but the first element is used to hold its
 ;; type as returned by `type-of'; records used to create new types that are not
 ;; built into Emacs

 (setq r (record 'foo 23 [bar baz] "rats"))


 ;; predicates

 (recordp r)

)


;; type descriptors

(comment

 ;; a type descriptor is a record which holds information about a type; slot 1
 ;; in the record must be a symbol naming the type, and `type-of' relies on this
 ;; to return the type of record objects; no other type descriptor slot is used
 ;; by Emacs, but they are free for use by Lisp extensions (user code?)

 ;; what's next?
 (setq td (record 'foo 'bar))

)


;; autoload type

(comment

 ;; an autoload object is a list whose first element is the symbol 'autoload; it
 ;; is stored as the function definition of a symbol, where it serves as a
 ;; placeholder for the real definition, containing the information about the
 ;; real definition (name of the file of Lisp code and some other info) so it
 ;; can be loaded when necessary

 ;; after the file with definition has been loaded, the symbol should have a new
 ;; function definition that is not an autoload object; the new definition is
 ;; then called as if it had been there to begin with

 ;; an autoload object is usually created with the function `autoload', which
 ;; stores the object in the function cell of the symbol

 (autoload 'foo "02-lisp-data-types.el")
 (symbol-function 'foo)
 (defun foo (x) (* x x))

)


;; finalizer type

(comment

 ;; a finalizer object helps Lisp code clean up after objects that are no longer
 ;; needed; when a finalizer object becomes unreachable after a garbage
 ;; collection pass (references from finalizers do not count as reachable),
 ;; Emacs calls the finalizer's associated function object (exactly once, even
 ;; if it fails), and print errors to *Messages*

 (make-finalizer (lambda () (print "Done!")))

)


;;;; editing types

;; buffer type

(comment

 ;; a buffer is an object that holds text that can be edited

 ;; most buffers hold the contents of a disk file so they can be edited, but
 ;; some are used for other purposes; most buffers are also meant to be seen by
 ;; the user, and therefore displayed, at some time, in a window (but they need
 ;; not be, in general)

 ;; each buffer has a designated position called "point"; most editing commands
 ;; act on the contents of the current buffer in the heighborhood of point; at
 ;; any time, one buffer is the current buffer

 ;; the contents of a buffer are much like a string, but buffers are not used
 ;; like strings, and the available operations are different; also buffers are
 ;; mutable, while strings are not

 ;; several data structures are associated with each buffer:
 ;; - a local syntax table
 ;; - a local keymap
 ;; - a list of buffer-local variable bindings
 ;; - overlays
 ;; - text properties for the text in the buffer

 ;; the local keymap and variable list contain entries that individually
 ;; override global bindings or values; these are used to customize the behavior
 ;; of programs in different buffers (hold part of the programs' local state?)
 ;; without actually changing the programs

 ;; a buffer may be "indirect" --- share the text of another buffer, but present
 ;; it differently

 ;; buffers have no read syntax, and print in hash notation with buffer name

 (current-buffer)


 ;; predicates

 (bufferp (current-buffer))

)


;; marker type

(comment

 ;; a marker denotes a position in a specific buffer; they have two components:
 ;; one for the buffer, and one for the position

 ;; changes in the buffer's text automatically relocate the position value as
 ;; necessary to ensure that the marker always points between the same two
 ;; characters in the buffer

 (point-marker)


 ;; predicates

 (markerp (point-marker))
 (number-or-marker-p (point-marker))

)


;; window type

(comment

 ;; a window describes the portion of the screen that Emacs uses to display
 ;; buffers

 ;; every live window has one associated buffer, whose contents appear in that
 ;; window, but a buffer may appear in one window, several windows, or no window
 ;; at all

 ;; windows are grouped on the screen into frames; each window belongs to one
 ;; and only one frame

 ;; at any time one window is designated the "selected window"; this is the
 ;; window where the cursor is (usually) displayed when Emacs is ready for a
 ;; command; the selected window usually displays the current buffer, but this
 ;; is not necessarily the case

 ;; windows have no read syntax, and print in hash notation with the window
 ;; number and buffer name

 (selected-window)


 ;; predicates

 (windowp (selected-window))
 (window-live-p (selected-window))

)


;; frame type

(comment

 ;; a frame is a screen area that contains one or more Emacs windows; the term
 ;; "frame" is also used to refer to the Lisp object that represents this area

 ;; frames have no read syntax, and print in hash notation with frame's title
 ;; and memory address (as an unique identifier)

 (selected-frame)


 ;; predicates

 (framep (selected-frame))
 (framep-on-display (selected-frame))
 (frame-live-p (selected-frame))

)


;; terminal type

(comment

 ;; a terminal is a device capable of displaying one or more Emacs frames

 ;; terminals have no read syntax, and print in hash notation with terminal's
 ;; ordinal number and its TTY device file name

 (get-device-terminal nil)

)


;; window configuration type

(comment

 ;; a window configuration stores information about the positions, sizes, and
 ;; contents of the windows in a frame, so the same arrangement can be recreated
 ;; later

 ;; window configurations have no read syntax, and print in hash notation

 (current-window-configuration)


 ;; predicates

 (window-configuration-p (current-window-configuration))

)


;; frame configuration type

(comment

 ;; a frame configuration stores information about the positions, sizes, and
 ;; contents of the windows in all frames; it is not a primitive type --- it is
 ;; a list whose CAR is 'frame-configuration and whose CDR is an alist, where
 ;; each element describes one frame

 (current-frame-configuration)
 (car (cdr (current-frame-configuration)))


 ;; predicates

 (frame-configuration-p (current-frame-configuration))

)


;; process type

(comment

 ;; generally, a process is a running program; in Emacs Lisp, a process is a
 ;; Lisp object that designates a subprocess created by the Emacs process;
 ;; running programs (shells, debuggers, network clients, compilers etc) as
 ;; subprocesses extend the capabilities of Emacs

 ;; a subprocess takes textual input from Emacs and returns textual output to
 ;; Emacs for further manipulation; Emacs can also send signals to the
 ;; subprocess

 ;; process objects have no read syntax, and print in hash notation with the
 ;; name of the process

 (process-list)


 ;; predicates

 (processp (car (process-list)))

)


;; thread type

(comment

 ;; a thread in Emacs represents a separate thread of Emacs Lisp execution; it
 ;; runs its own Lisp program, has its own current buffer, and can have
 ;; subprocesses locked to it, i.e., subprocesses whose output only this thread
 ;; can accept

 ;; threads have no read syntax, and print in hash notation with the name of the
 ;; thread, if any, or its address in memory

 (all-threads)


 ;; predicates

 (threadp (car (all-threads)))

)


;; mutex type

(comment

 ;; a mutex is an exclusive lock that threads can own and disown, in order to
 ;; synchronize between them

 ;; mutexes have no read syntax, and print in hash notation with the name of the
 ;; mutex, if any, or its address in memory

 (setq m (make-mutex "my-mutex"))
 (make-mutex)


 ;; predicates

 (mutexp m)

)


;; condition variable type

(comment

 ;; a condition variable is a device for a more complex thread synchronization
 ;; than the one supported by a mutex; a thread can wait on a condition
 ;; variable, to be woken up when some other thread notifies the condition

 ;; condition variables have no read syntax, and print in hash notation with the
 ;; name of the condition variable, if any, or its address in memory

 (setq cvar (make-condition-variable (make-mutex)))
 (make-condition-variable (make-mutex) "my-condition-variable")


 ;; predicates

 (condition-variable-p cvar)

)


;; stream type

(comment

 ;; a stream is an object that can be used as a source or sink for characters
 ;; --- either to supply characters for input or to accept them as output; many
 ;; different types can be used this way: markers, buffers, strings, and
 ;; functions; most often, input streams (character sources) obtain characters
 ;; from the keyboard, a buffer, or a file, and output streams (character sinks)
 ;; send characters to a buffer or to the echo area

 ;; the object `nil' may be used as a stream --- it stands for the value of the
 ;; variable `standard-input' or `standard-output'; also, the object `t' as a
 ;; stream specifies input using the minibuffer or output in the echo area

 ;; streams have no special printed representation or read syntax, and print as
 ;; whatever primitive type they are

 standard-input
 standard-output

)


;; keymap type

(comment

 ;; a keymap maps keys typed by the user to commands; it is actually a list
 ;; whose CAR is the symbol 'keymap

 (setq km (make-sparse-keymap "my-sparse-keymap"))
 (make-keymap "my-keymap")


 ;; predicates

 (keymapp km)

)


;; overlay type

(comment

 ;; an overlay specifies properties that apply to a part of a buffer

 ;; each overlay applies to a specified range of the buffer, and contains a
 ;; property list; overlay properties are used to present parts of the buffer
 ;; temporarily in a different display style

 ;; overlays have no read syntax, and print in hash notation with the buffer
 ;; name and range of positions

 (setq o (make-overlay 0 3))


 ;; predicates

 (overlayp o)

)


;; font type

(comment

 ;; a font specifies how to display text on a graphical terminal

 ;; there are actually three separate font types --- font objects, font specs,
 ;; and font entities --- each of which has slightly different properties

 ;; none of font type objects have read syntax, and print in hash notation

 ;; a loaded font
 (setq fnt (font-at 1))
 ;; font specification, to match fonts
 (font-spec)
 ;; a font matching by font specification but not necessarily loaded
 (find-font (font-spec))


 ;; predicates

 (fontp fnt)

)


;; xwidget type

(comment

 ;; an xwigdet is a special display element, such as web browser, that can be
 ;; embedded inside a buffer

 ;; each window that displays an xwigdet will also have an "xwigdet view", which
 ;; on X-Windows corresponds to a single X window used to display the widget

 ;; neither of these objects have read syntax, and print in hash notation

 (setq w (make-xwidget 'webkit "Hello xwidget!" 300 400))
 (xwidget-webkit-goto-url "http://google.com")
 (kill-xwidget w)


 ;; predicates

 (xwidgetp w)
 (xwidget-live-p w)

)


;;;; read syntax for circular objects

;; was already dealt with in Special read syntax above


;;;; type predicates

;; the Emacs Lisp interpreter does not perform type checking on the actual
;; arguments passed to functions; the function should check their types

;; all built-in functions do check the types of their actual arguments and
;; signal a `wrong-type-argument' error if necessary

;; the most common way to check the type is to call a predicate function, that
;; takes one argument and returns `t' or `nil'

(comment

 (+ 2 'a)

 (symbolp 'a)

)


;;;; equality predicates

(comment

 ;; tests if objects are the same object
 (setq v [1 2 3])
 (eq v v)
 (eq 'a 'a)
 (eq ?A ?A)
 (eq 1 1)

 ;; compares values
 (equal v v)
 (equal [1 2 3] v)
 (equal "foo" "foo") ; does not consider properties
 (equal (point-marker) (point-marker))

 (equal-including-properties "foo" "foo") ; considers properties

)


;;;; mutability

;; some Lisp objects should never change, and some can be (but not always
;; should) be changed --- they are mutable

;; for example, a mutable object stops being mutable if it is given to `eval';
;; attempts to mutate objects that should not be mutated is undefined behavior
;; (but currently not a error)
