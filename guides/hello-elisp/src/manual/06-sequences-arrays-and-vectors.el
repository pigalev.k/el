;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; the sequence type is the union of two other types: lists and arrays

;; an array is a fixed-length object with a sot for each of its elements, that
;; are accessible in constant time

;; a list is a sequence of elements made of cons cells, so access is in linear
;; time; but elements can be added and removed


;;;; sequences

(comment

 (sequencep '(1 2 3))
 (sequencep "foo")

 (length '(1 2 3 4 5))
 (length [1 2 3])
 (length< [1 2 3] 10)
 (length> [1 2 3] 1)

 (setq s1 '(1 2 3))
 (elt s1 2)

 ;; shallow copy: the elements are the same
 (setq s2 (copy-sequence s1))
 (setq s3 (copy-sequence [1 2 3 4]))

 ;; returns a new sequence
 (reverse s1)
 ;; returns reversed and modifies in-place
 (setq s2 (nreverse s2))
 (nreverse s3)

 (setq nums (list 2 1 4 3 0))
 ;; modifies `nums'
 (setq nums (sort nums #'<))


 ;; more functions: `seq.el' (pure, generic)

 (require 'seq)

 (setq s4 [1 2 3 4])
 (defun even? (x)
   (= 0 (% x 2)))

 (seqp s4)
 (seq-elt s4 2)
 (seq-length s4)
 (seq-drop s4 2)
 (seq-take s4 2)
 (seq-take-while (lambda (x) (< x 3)) s4)
 (seq-drop-while (lambda (x) (< x 3)) s4)
 (seq-split s4 2)
 (seq-do #'print s4)
 (seq-map #'1+ s4)
 (seq-map-indexed #'+ s4)
 (seq-mapn #'* s4 s4 s4)
 (seq-filter #'even? s4)
 (seq-remove #'even? s4)
 (seq-remove-at-position s4 2)
 (seq-keep #'even? s4)
 (seq-reduce #'+ s4 0)
 (seq-some #'even? s4)
 (seq-find #'even? s4)
 (seq-every-p #'even? s4)
 (seq-empty-p s4)
 (seq-count #'even? s4)
 (seq-sort #'> s4)
 (seq-sort-by #'seq-first #'< [[3] [1] [0] [2]])
 (seq-contains-p s4 2)
 (seq-set-equal-p s4 (seq-sort #'> s4))
 (seq-position s4 2)
 (seq-positions s4 2)
 (seq-uniq s4)
 (seq-subseq s4 1 3)
 (seq-concatenate 'list s4 s4)
 (seq-mapcat #'seq-reverse '((3 2 1) (6 5 4)))
 (seq-partition '(0 1 2 3 4 5 6 7) 3)
 (seq-union [1 2 3] [3 4])
 (seq-intersection [2 3 4 5] [1 3 5 6 7])
 (seq-difference '(2 3 4 5) [1 3 5 6 7])
 (seq-group-by #'integerp '(1 2.1 3 2 3.2))
 (seq-into [1 2 3] 'list)
 (seq-min s4)
 (seq-max s4)
 (seq-doseq (i [1 2 3 4])
   (prin1 i))
 ;; destructuring binding
 (seq-let [first second &rest more] [1 2 3 4]
     (list more first second))
 (let ((a nil)
       (b nil))
   (seq-setq (_ a _ b) '(1 2 3 4))
   (list a b))
 (seq-random-elt [1 2 3 4])

)


;;;; arrays

;; the first element of an array has index zero

;; the length of an array is fixed

;; the array is a constant (evaluates to itself)

;;;; functions that operate on arrays

(comment

 (setq a1 [1 2 3 4 5 6 7])

 (arrayp a1)
 (aref a1 3)
 (aset a1 0 0)
 (fillarray (copy-sequence a1) 'x)

)


;;;; vectors

;; vector is a general-purpose array

;;;; functions for vectors

(comment

 (setq v1 [1 2 3 4 5 6 7])

 (vectorp v1)
 (vector 1 2 3 4)
 (make-vector 3 'x)
 (vconcat '(a b c) '(d e f))

 (append v1 nil)

)


;;;; char-tables

;; a char-table is a specialized array, indexed by character codes

;; each char-table has a subtype --- a symbol, that provides an easy way to tell
;; its purpose and controls the number of additional slots

;; a char-table can have a parent, another char-table, where missing mappings
;; will be looked up; and char-table can have default value that will be
;; returned if no value can be found for a specific key

(comment

 (setq ct1 (make-char-table 'foo))

 (char-table-p ct1)
 (char-table-subtype ct1)
 (char-table-parent ct1)
 (set-char-table-parent ct1 (current-case-table))
 ;; no extra slots (char-table-extra-slot ct1 0)
 ;; (set-char-table-extra-slot ct1 0 'bar)
 (char-table-range ct1 nil)
 (char-table-range ct1 '(0 . 10))
 (char-table-range ct1 ?x)

 (map-char-table (lambda (key val)
                   (prin1 (format "%s %s" key val))) ct1)

)


;;;; bool-vectors

;; bool-vectors are specialized arrays that can store only t and nil

(comment

 (setq bv1 (make-bool-vector 8 1))
 (setq bv2 (bool-vector 1 1 1 nil nil nil nil nil))

 (bool-vector-p bv1)
 (bool-vector-exclusive-or bv1 bv2)
 (bool-vector-union bv1 bv2)
 (bool-vector-intersection bv1 bv2)
 (bool-vector-set-difference bv1 bv2)
 (bool-vector-not bv2)
 (bool-vector-subsetp bv2 bv1)
 (bool-vector-count-consecutive bv2 nil 0)
 (bool-vector-count-population bv2)

)


;;;; managing a fixed-size ring of objects

;; a ring is a fixed-size data structure that supports insertion, deletion,
;; rotation, and modulo-indexed reference and traversal

(comment

 (require 'ring)

 (setq r1 (make-ring 5))

 (ring-p r1)
 (ring-size r1)
 (ring-length r1)
 (ring-elements r1)
 (setq r2 (ring-copy r1))
 (ring-empty-p r2)
 (ring-insert r1 :a)
 (ring-ref r1 0)
 (ring-remove r1 0)
 (ring-insert-at-beginning r1 :a)
 (ring-resize r1 3)

)
