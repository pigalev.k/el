;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; users can customize variables and faces without writing Lisp code, by using
;; the Customize interface; the customization items can be defined that users
;; can interact with --- customizable variables, customizable faces and
;; customizable groups (containers for the previous two)


;;;; common item keywords

;; lots of reference material, no theory, no guidelines, no code; maybe it's
;; time to just read the fine manual (again)? What? So I thought

;;;; defining customization groups

;; each package should have one main customization group, and maybe several
;; subgroups if there are many items; the main group should be a member of some
;; standard customization group(s)

(comment

 (defgroup foo-mode nil
   "A customization group for the package `foo.el'."
   :tag "Foo mode"
   :group 'convenience
   :prefix "foo-"
   :link '(custom-manual "foo")
   :version "30.0")

 custom-unlispify-remove-prefixes

)

;;;; defining customization variables

(comment

 (defcustom foo-global-options nil
   "Command line options used to execute `foo'."
   :tag "Global options for foo"
   :group 'foo-mode
   :type 'string
   :safe #'stringp
   :package-version '(foo-mode . "0.1.0"))

)

;;;; customization types

;; reams of reference material again; never wanted such a sprawling mouse-driven
;; interface anyway, if only for option discovery

;;;; applying customizations

(custom-set-variables)
(custom-set-faces)

;;;; custom themes

;; custom themes are collections of settings that can be enabled and disabled as
;; a unit

;; each theme should reside in one source file named `foo-theme.el', where `foo'
;; is the theme name; the first form should be a call to `deftheme', and the
;; last form should be a call to `provide-theme'

(comment

 custom-known-themes

)
