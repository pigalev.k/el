;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; records allows to create user-defined types

;; internally, record is like a vector, with the first slot holding its type
;; (symbol or a type descriptor), max 4096 slots

;; a record is a constant for evaluation (evaluates to itself)


;;;; record functions

(comment

 (setq rec1 (record 'foo 1 2 :bar [3 4] "oops"))
 (setq rec2 (make-record 'bar 9 'X))

 (recordp rec1)
 (aref rec1 3)
 (aset rec1 3 :barrr)
 rec1

)


;;;; backward compatibility

;; royally irrelevant
