;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;;;; what is a function?

;; a function is a rule for carrying out a computation given input values
;; (arguments); the result of the computation is the value (the return value) of
;; the function; the computation can also have side effects

;; a pure function is a function without side effects and referentially
;; transparent (always returns the same value for the same arguments)

;; in Lisp, a function is just another Lisp object; certain function-like
;; objects, like special forms and macros, also accept arguments to carry out
;; computations

(comment

 (functionp 'car)
 (func-arity 'car)

 (subrp (symbol-function 'car))
 (subr-arity (symbol-function 'car))

 (byte-code-function-p (symbol-function 'next-line))
 (compiled-function-p (symbol-function 'next-line))
 (type-of (symbol-function 'next-line))

)


;;;; lambda expressions

;; a lambda expression is a function object written in Lisp (an expression that
;; evaluates to a function object)

;;;; components of a lambda expression

;; - symbol `lambda'

;; - argument list (lambda list)

;; - documentation string (optional, but strongly recommended)

;; - interactive declaration (optional): how to provide arguments if called
;;   interactively --- for commands

;; - body forms

;;;; features of argument lists

;; - `&optional' before optional arguments
;; - `&rest' before a list of zero or more extra arguments

;; omitted optional arguments default to nil; arguments are bound in order ---
;; required arguments always precede optional ones; no arguments will be bound
;; after the rest

;;;; a simple lambda expression example

(comment

 (setq square
       (lambda (x)
         "Returns X squared.

\(square number)"
         (* x x)))

 (funcall square 4)


 (setq say (lambda
             (a b &optional c &rest more)
             "Formats the arguments as string."
             (format "%s, %s, and %s, and then %s" a b c more)))

 (funcall say 1 2)
 (funcall say 1 2 3 4 5 6 7)

)

;;;; documentation strings of functions

;; the first line of the docstring should stand on its own (for apropos);
;; arglist description could be overridden in docstring; docstring could be
;; dynamic

;;;; naming a function

;; symbols can name functions; symbol whose function cell contains a function
;; object can be used as a function; `defun' is a usual way to name functions

;;;; defining functions

(comment

 ;; overrides arglist description in the help
 (defun cube (x)
   "Returns NUMBER cubed.

\(fn NUMBER)"
   (* x x x))

 ;; dynamic documentation string (requires lexical bindings enabled)
 (defun adder (x)
   "Returns a function that adds X to its argument."
   (lambda (y)
     (:documentation (format "Add %S to the argument Y." x))
     (+ x y)))

 (defalias 'adder5 (adder 5))
 (documentation 'adder5)

 ;; aliasing may create whole chains of symbols pointing to the function object
 ;; or store the function object directly in the symbol's function cell

 (defalias '5+ 'adder5)
 (function-alias-p '5+)
 (documentation '5+)

 (fset 'plus5 (symbol-function 'adder5))
 (function-alias-p 'plus5)
 (documentation 'plus5)

 ;; `defalias' records which file defined the function, `fset' does not

)


;;;; calling functions

;; the most common way to call a function is by evaluating a list; use `funcall'
;; when the function to call is computed at runtime; use `apply' to determine
;; how many arguments to pass at runtime

;; functions that accept function arguments are called functionals

(comment

 ;; cannot call macros or special forms this ways --- they need their arguments
 ;; unevaluated

 (funcall 'car '(1 2 3))

 (apply '+ '(1 2 3))
 ;; special (one-argument) case
 (apply '(+ 3 4))


 (defalias '6+ (apply-partially '+ 1 2 3))
 (6+ 1)

 (identity 2)
 (ignore 2)
 (always 2)

)


;;;; mapping functions

;; a mapping function applies a given function to each element of a list or
;; other collection

(comment

 (mapcar 'car '((a b) (c d) (e f)))
 (mapcar '1+ [1 2 3])
 (mapcar 'string "abc")
 (mapcar 'identity (bool-vector t t nil t))

 (mapcar #'list '(a b c d))
 (mapcan #'list '(a b c d))

 (mapc 'princ '(1 2 3))

 (mapconcat 'list '(98 99 100) ",")

)


;;;; anonymous functions

;; can be constructed with `lambda', `function' or #' read syntax

(comment

 ;; all equivalent (a closure under lexical binding)

 (lambda (x) (* x x))
 ;; like `quote', but on byte-compilation its argument will be compiled into a
 ;; byte-code function object
 (function (lambda (x) (* x x)))
 #'(lambda (x) (* x x))

)


;;;; generic functions

;; a generic (polymorphic) function is a set of functions with the same name but
;; different implementations for different sets of arguments (e.g., arguments of
;; different types); which of the functions is actually called is decided at
;; runtime

;; a generic function specifies an abstract operation by defining its name and
;; list of arguments, but (usually) no implementation; actual implementations
;; for several classes of arguments are provided by methods, which are defined
;; separately

;; each method has the same name as the generic function, but the method's
;; definition indicates what kinds of arguments it can handle by specializing
;; the arguments defined by the generic function; these argument specializers
;; can be more or less specific

;; when a generic function is invoked, it selects the applicable methods by
;; comparing the actual arguments with the arguments specializers of each method
;; (does dispatch); a method is applicable if the actual arguments of the call
;; are compatible with the method's specializers; if more than one method is
;; applicable, they are combined using certain rules, and the combination then
;; handles the call

;; it is quite complex a topic, see the manual and Common Lisp docs for the gory
;; details

(comment

 ;; checking accounts can have a negative balance; savings accounts can't
 (cl-defstruct checking-account balance)
 (cl-defstruct savings-account balance)

 ;; a generic `withdraw' method
 (cl-defgeneric withdraw (account amount)
   (:documentation "Withdraw AMOUNT from ACCOUNT balance.
Give an error for savings accounts where balance < AMOUNT")
   ;; default implementation
   (format-message "Don't know how to withdraw from `%s'" account))

 ;; implementation of `withdraw' for checking accounts
 (cl-defmethod withdraw ((account checking-account) amount)
   (cl-decf (checking-account-balance account) amount))

 ;; implementation of `withdraw' for savings accounts
 (cl-defmethod withdraw ((account savings-account) amount)
   (when (< (savings-account-balance account) amount)
     (user-error "Account overdrawn."))
   (cl-decf (savings-account-balance account) amount))

 ;; implementation of `withdraw' for an read-only account represented by a
 ;; number
 (cl-defmethod withdraw ((account number) amount)
   (format "Account is read-only, balance: %d" account))

 ;; account instantiations, i.e. creation of account objects
 (setq x (make-checking-account :balance 50))
 (setq y (make-savings-account :balance 50))

 ;; withdraw amounts from accounts
 (withdraw x 60)
 (withdraw y 60)
 (withdraw 123 12)
 (withdraw (current-buffer) 1)

)


;;;; accessing function cell contents

(comment

 ;; void function cell, can't be called
 (symbol-function 'nope)
 (fboundp 'nope)
 (nope)

 (symbol-function 'car)

 (defalias 'first 'car)
 (symbol-function 'first)
 ;; follows the function indirection chain down to the actual function
 (indirect-function 'first)

 (fmakunbound 'first)
 (fboundp 'first)

 (fset '1st (symbol-function 'car))
 (1st '(2 3 4))

 ;; define a named keyboard macro
 (fset 'kill-two-lines "\^u2\^k")
 (execute-kbd-macro 'kill-two-lines)

)


;;;; closures

;; a closure is a function with a lexical environment that existed when the
;; function was defined

;; when lexical binding is enabled, any function that you create (named or
;; anonymous) will be automatically converted into a closure

;; a closure can access and modify its lexical environment any time it is
;; invoked

;; a closure is represented by a list that contains the symbol 'closure, a list
;; representing the lexical environment, and the argument list and body of the
;; function

(comment

 (let ((v 3))
   (setq fun (lambda (x) (* x v))))

 (funcall fun 2)

)


;;;; open closures

;; open closure is a function object which carries additional type information
;; and expose it in the form of slots which can be accessed via accessor
;; functions

(comment

 (oclosure-define a-kbd-macro
   "A keyboard macro."
   keys (counter :mutable t))

 (defun a-kbd-macro (key-sequence)
   (oclosure-lambda (a-kbd-macro (keys key-sequence) (counter 0))
       (&optional arg)
     (interactive "P")
     (execute-kbd-macro keys arg)
     (setq counter (1+ counter))))

 ;; can be called as ordinary lambda
 (setq m1 (a-kbd-macro "\^u2\^p"))
 (funcall m1)

 ;; but not so opaque
 (oclosure-type m1)
 (a-kbd-macro--keys m1)
 (a-kbd-macro--counter m1)

)


;;;; advising Emacs Lisp functions

;; the advice feature allows to add to the existing definition of a function
;; instead of replacing it altogether (advise it); a way to compose functions

;; beware: advice may break existing callers of the advised function; it may be
;; better to write a new command instead or use existing extension points (e.g.,
;; hooks)

(comment

 ;; high-level interface: `advice-add' and `advice-remove'; manipulate functions
 ;; by their names; can advise macros and autoloaded functions, preserve the
 ;; original docstring as well as document the advice, can add advice even
 ;; before the function is defined

 (defun my-double (x)
   (* x 2))

 (defun my-increase (x)
   (+ x 1))

 (advice-add 'my-double :filter-return #'my-increase)
 (my-double 2)

 (advice-remove 'my-double #'my-increase)
 (my-double 2)


 ;; core interface: `add-function' and `remove-function'; manipulate function
 ;; objects

 (defun my-tracing-function (proc string)
   (message "Proc %S received %S" proc string))

 ;; M-x shell
 (setq proc (car (process-list)))
 (add-function :before (process-filter proc) #'my-tracing-function)
 (remove-function (process-filter proc) #'my-tracing-function)

)

;; not sure if these gory details should be covered here in depth; probably not

;;;; primitives to manipulate advices

;;;; advising named functions

;;;; ways to compose advice

;;;; adapting code using the old `defadvice'

;;;; advice and byte code


;;;; declaring functions obsolete

(comment

 (defun old-square (x)
   "Squares the X the old way."
   (* x x))

 (defun new-square (x)
   "Squares the X the new way."
   (* x x))

 (make-obsolete 'old-square 'new-square "30.0")
 (describe-function 'old-square)

 ;; the same, but in one step
 (define-obsolete-function-alias 'old-square 'new-square "30.1")
 (describe-function 'old-square)

 (set-advertised-calling-convention 'new-square '(number) "30.2")
 (describe-function 'new-square)

)


;;;; inline functions

;; when a call to an inline function is byte-compiled, the function's definition
;; is expanded into the caller

;; may be faster, but requires recompilation of all callers if changed

(comment

 (defsubst inline-square (x)
   "Squares X. Will be inlined on byte-compilation."
   (* x x))

 ;; also `define-inline', but it is quite hairy, see the manual if you must

)


;;;; the `declare' form

;; a macro used to add metadata to a function or macro

(comment

 (defun weird-square (x)
   "Squares NUM as a whacko."
   ;; more such stuff in the manual
   (declare (advertised-calling-convention '(NUM) "30.0")
            (obsolete "use with caution." "23.1")
            (pure t)
            (side-effect-free t)
            (speed 3))
   (* x x))

 (describe-function 'weird-square)

)


;;;; telling the compiler that a function is defined

;; byte-compiler often issues warnings about functions that is not defined (yet;
;; but they possibly will be when the code will be run and will load other
;; files)

(comment

 ;; to suppress such warnings

 (declare-function shell-mode "shell.el")

 (check-declare-file "13-functions.el")
 (check-declare-directory ".")

)


;;;; determining whether a function is safe to call

;; some major modes call functions from user files, and that is risky

(comment

 ;; quick and dirty check, there may be many false positives
 (unsafep '(+ 1 2))
 (unsafep '(eval '()))

)


;;;; other topics related to functions

;; just an incomplete index of related functions, most already seen
