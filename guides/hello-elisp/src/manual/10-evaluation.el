;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; the evaluation of expressions is performed by the Lisp interpreter --- a
;; program that receives a Lisp object as input and computes its value,
;; according to evaluation rules; the interpreter runs automatically to evaluate
;; portions of a program, but can also be called explicitly via primitive
;; function `eval'


;;;; introduction to evaluation

;; a Lisp object that is intended for evaluation is called a form or expression
;; (S-expression, sexp), i.e., forms are data, not text; reading text (printed
;; representations of objects) into data structures (objects) and evaluating the
;; data structures are separate activities

;; evaluation is a recursive process --- evaluating a form may involve
;; evaluating parts within this form, and applying functions (primitive and not)
;; to their arguments

;; evaluation takes place in a context called the environment, which consists of
;; current values and bindings of all Lisp variables; a variable evaluates to
;; the value given by the current environment; evaluating a form may also
;; (temporarily) alter the environment by binding local variables

;; evaluating a form may also make changes that persist --- side effects (e.g.,
;; setting values of global variables)


;;;; kinds of forms

;; evaluation rules for a form depend on its data type; three kinds of forms are
;; evaluated differently --- symbols, lists, and all other types
;; (self-evaluating forms)

;;;; self-evaluating forms

;; any form that is not a list or a symbol --- number, string, vector, etc

(comment

 ;; quoting make no difference

 '123
 123
 (eval 123)

)


;;;; symbol forms

(comment

 ;; symbols are evaluated as variables; the result of evaluation is the
 ;; variable's value, or error if symbol has no value

 (setq a 123)
 (eval a)
 a

 ;; evaluation signals an error
 unbound-variable

 ;; symbols `nil', `t' and keywords (symbols starting with colon) are treated
 ;; specially: they evaluate to themselves and cannot be rebound

 nil
 t
 :abc

)


;;;; classification of list forms

;; nonempty list is either a function call, a macro call, or a special form,
;; depending solely on its first element; the first element is NOT evaluated in
;; Emacs Lisp (as it would be in some other Lisps, e.g., Scheme)


;;;; symbol function indirection

;; if the first element of the list is a symbol, its function cell's contents
;; are used instead of the symbol; if the contents are another symbol, this
;; process (symbol function indirection) is repeated until it obtains a
;; non-symbol --- a function or other suitable object: a Lisp function (lambda
;; expression), a Lisp macro, a special form, or an autoload object; else an
;; error is signaled

(comment

 ;; indirection
 (fset 'first 'car)
 (fset 'erste 'first)
 (erste '(1 2 3))

 ;; no indirection
 ((lambda (arg) (erste arg)) '(1 2 3))
 ;; recommended approach
 (funcall (lambda (arg) (erste arg)) '(1 2 3))


 (indirect-function 'erste)

)


;;;; evaluation of function forms

;; if the first element of a list being evaluated is a Lisp function object,
;; byte-code object or primitive function object, then that list is a function
;; call

;; remaining elements of the list are evaluated first (applicative-order
;; evaluation) from left to right; then the function is called with these
;; argument values (using `apply')

;; if the function is a Lisp function, the argument values are bound to argument
;; variables of the function, and the forms in the function body are evaluated
;; in order, returning the value of the last body form

(comment

 (+ 1 2)

)


;;;; Lisp macro evaluation

;; if the first element of a list being evaluated is a macro object, the list is
;; a macro call

;; the remaining elements of the list are NOT initially evaluated (normal-order
;; evaluation); the macro computes a replacement form (expansion of the macro)
;; to be evaluated in place of the original form; if the expansion is itself a
;; macro call, expansion repeats until some other sort of form results

(comment

 (macroexpand-all '(when t 1))

)


;;;; special forms

;; a special form is a primitive specially marked so that its arguments are not
;; all evaluated; most special forms define control structures or perform
;; variable bindings --- things that functions cannot do

;; each special form has its own rules for which arguments are evaluated and
;; which are used without evaluation; whether a particular argument is evaluated
;; may depend on the results of evaluating other arguments

;; if an expression's first symbol is that of a special form, the expression
;; should follow the rules of that form (be well-formed), or results of
;; evaluation are not well-defined

(comment

 (special-form-p 'if)

)


;;;; autoloading

;; the autoload feature allows to call a function or macro whose definition has
;; not yet been loaded into Emacs; it specifies which file contains the
;; definition; when an autoload object appears as a symbol's function
;; definition, calling that symbol as a function automatically loads the
;; specified file, then calls the real definition loaded from that file, as if
;; it was defined from the start


;;;; quoting

;; the special form `quote' returns its single argument without evaluating; this
;; provides a way to include constant symbols and lists (which are not
;; self-evaluating) in a program's text

(comment

 (quote (1 2 3))
 ;; read syntax
 '(1 2 3)
 ;; expands to a list (quote ...) at read time
 (car (read "'(1 2 3)"))

)


;;;; backquote

;; backquote allows to quote a list, but selectively evaluate elements of that
;; list

(comment

 ;; the same
 '(+ 1 2 3)
 (quote '(+ 1 2 3))
 `(+ 1 2 3)
 (backquote (+ 1 2 3))

 ;; comma unquotes a value
 `(+ 1 2 ,(+ 1 2))
 (backquote (+ 1 2 ,(+ 1 2)))

 ;; comma+at-sign unquotes the value and splices it (replaces with a sequence of
 ;; its elements); value should be a list

 `(+ 1 2 ,@(list 3 4 5))

)


;;;; eval

;; `eval' can be used for deliberate evaluation of forms (e.g. composed at run
;; time)

;; since `eval' is a function, its argument is evaluated twice (before call and
;; in the process of the call)

(comment

 max-lisp-eval-depth

 (eval '(+ 1 2 3))
 (eval-region 1 (point-max) standard-output)
 (eval-buffer nil t)

)


;;;; deferred and lazy evaluation

;; evaluation of expression can be delayed until the value is actually needed
;; (lazy computations)

(comment

 (require-with-shorthand 'thunk '("t-" . "thunk-"))

 (setq t1 (t-delay (progn
                     (prin1 "done")
                     (+ 1 2 3))))

 ;; delayed expression evaluated once, the value is cached and returned on
 ;; subsequent forcings
 (t-force t1)

 ;; delayed expression evaluated each time `f' is called (with number > 10)
 (defun f (number)
   (t-let ((derived-number
            (progn (prin1 (format "Calculating 1 plus 2 times %d" number))
                   (1+ (* 2 number)))))
     (if (> number 10)
         derived-number
       number)))

 (f 5)
 (f 12)

 ;; delayed expressions can refer to each other; also evaluated each time
 (t-let* ((x (prog2 (prin1 "Calculating x...")
                     (+ 1 1)
                   (prin1 "Finished calculating x")))
              (y (prog2 (prin1 "Calculating y...")
                     (+ x 1)
                   (prin1 "Finished calculating y")))
              (z (prog2 (prin1 "Calculating z...")
                     (+ y 1)
                   (prin1 "Finished calculating z")))
              (a (prog2 (prin1 "Calculating a...")
                     (+ z 1)
                   (prin1 "Finished calculating a"))))
   (* z x))

 ;; presumably buffer-local, but values from other buffers can sometimes sneak
 ;; in, somebody better investigate soon
 read-symbol-shorthands

)
