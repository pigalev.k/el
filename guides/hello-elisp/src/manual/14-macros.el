;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; a macro is a function that instead of computing a value computes another Lisp
;; expression, which will compute a value (the expansion of the macro)

;; macros operate on the unevaluated expressions for the arguments and so can
;; construct the expansion from them


;;;; a simple example of a macro

(comment

 (defmacro ++ (var)
   "Increments the value of VAR and returns it."
   (list 'setq var (list '1+ var)))

 (setq a 1)
 (++ a)

 (macrop '++)

)


;;;; expansion of a macro call

;; when a macro call is evaluated, the macro is invoked with unevaluated
;; expressions for arguments; the returned value is another expression (macro
;; expansion); the expansion is then evaluated normally (expanding another macro
;; calls if necessary)

(comment

 (defmacro inc (var)
   (list 'setq var (list '1+ var)))

 (defmacro inc2 (var1 var2)
   (list 'progn (list 'inc var1) (list 'inc var2)))

 ;; expands the form until not a macro, but not subforms
 (macroexpand '(inc v))
 (macroexpand '(inc2 v1 v2))

 ;; expand the form once, but not subforms
 (macroexpand-1 '(inc2 v1 v2))

 ;; expands the form and its subforms until not macros
 (macroexpand-all '(inc2 v1 v2))

)


;;;; macros and byte compilation

;; macro calls might evaluate their expansions right away instead of handing
;; them over to evaluation, but this will break compilation --- effects of
;; expansion will happen at compile time instead of run time

;; byte compilation ensures that symbols defined by all `defmacro' forms in the
;; file being compiled are defined for the rest of the compilation process; it
;; also requires any top-level `require's in the file for the same reason; to
;; avoid loading the macro definitions when compiled program is being run, wrap
;; the `require's with macro definitions in `eval-when-compile'

;;;; defining macros

;; a macro object is a list whose CAR is the symbol 'macro, and whose CDR is a
;; function; expansion is applying the function (with `apply') to the list of
;; unevaluated arguments of the macro call

(comment

 ;; like this
 '(macro lambda args . body)

 )

;;;; common problems using macros

;;;; wrong time

;; doing some work prematurely --- while expanding the macro, rather than in the
;; expansion itself; with compilation it moves the work to compile time instead
;; of run time

;;;; evaluating macro arguments repeatedly

;; inadvertently evaluating arguments in the expansion more than once can lead
;; to problems, especially if these argument expressions have side effects

;;;; local variables in macro expansions

;; local variables defined in macro expansions may clash with variables defined
;; earlier; use uninterned symbols (e.g. created with `gensym') to avoid this

;;;; evaluating macro arguments in expansion

;; it will be done at the wrong time and will break compilation, and may shadow
;; variables defined earlier

(comment

 (defmacro foo (a)
   "Sets A to `t'."
   (list 'setq (eval a) t))

 (setq x 'b)
 ;; sets `b'
 (foo x)
 (macroexpand '(foo x))

 (setq a 'c)
 ;; sets `a', not `c'
 (foo a) ;; because symbol `a' in the expansion is the macro's argument name,
         ;; that shadows the user's variable `a', and its value is symbol `a'
 (macroexpand '(foo a))

)

;;;; how many times is the macro expanded?

;; sometimes a macro call is expanded each time it is evaluated, but only once
;; (during compilation) for a compiled function; if the macro definition has
;; side effects, it can create problems; solution is to avoid side effects in
;; macros (and on objects constructed by macro definitions too)


;;;; indenting macros

(comment

 (defmacro foo (x y z)
   (declare (indent 2))
   `(when ,x :foo))

 (foo
     t
     1
   2)

)
