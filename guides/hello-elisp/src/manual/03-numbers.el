;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; integer computations are exact, floating-point computations may involve
;; rounding errors


;;;; integer basics

(comment

 ;; integer literals
 1
 1.
 +1
 -1
 0
 +0
 -0

 ;; bases other than 10 (2-36)
 #b101100
 #o54
 #x2c
 #24r1k
 #36rxyz

 ;; supported value ranges
 most-positive-fixnum
 most-negative-fixnum
 integer-width

)


;;;; floating-point basics

(comment

 ;; the read syntax for floating-point numbers requires either nonempty fraction
 ;; part, an exponent, or both

 ;; literals
 1500.0
 +15e2
 15.0e+2
 +1500000e-3
 .15e4

 ;; infinity
 (/ 1 0.0)
 (/ -1 0.0)
 1.0e+INF

 ;; NaN; never numerically equal to anything, including itself
 (/ 0.0 0.0)
 0.0e+NaN
 (isnan 0.0e+NaN)


 (frexp 1.4)
 (ldexp 0.7 1)

 (copysign 1.0 -3.1)

 (logb 1024)

)


;;;; type predicates for numbers

;; see also Lisp Data Types

(comment

 (bignump 1)
 (fixnump 1)
 (floatp 1.0)
 (integerp 1)
 (numberp 1e2)
 (natnump 1)
 (zerop 0.0)

)


;;;; comparison of numbers

;; numerical equality is tested with `='; but do not directly compare floats, as
;; different rounding errors can make conceptually the same values slightly
;; differ

(comment

 ;; by value
 (= 1 1)
 (= 1 1.0)
 (/= 1 2)
 (/= 1 1)

 ;; numbers: by type and value, else as `eq'
 (eql 1.0 1)
 (eql 1 1)

 (< 1 2)
 (> 3 4)
 (>= 1 1)
 (<= 3 8)

 (max 1 2 10 -1 0 4 2)
 (min 1 2 10 -1 0 4 2)

 (abs -1)

)


;;;; numeric conversions

(comment

 (float 1)

 (truncate 1.2)
 (floor 1.2)
 (ceiling 1.2)
 (round 1.2)
 (round 1.7)

 ;; integer division (rounding the result)
 (round 13.1 4)
 (round 15.1 4)

)


;;;; arithmetic operations

(comment

 (1+ 1)
 (1- 1)

 (+)
 (+ 1 2 3 4)

 (-)
 (- 1 2 3 4)

 (*)
 (* 1 2 3 4)

 (/ 2.0)
 (/ 25 3 2 2)


 (% 13 -4)
 ;; like `%', but permits floating-point arguments and has the same sign as
 ;; divisor
 (mod 13 -4)
 (mod 5.5 2.5)

)


;;;; rounding operations

(comment

 ;; take a floating-point argument and return a floating-point result whose
 ;; value is a nearby integer

 (ffloor 1.2)
 (fceiling 1.2)
 (ftruncate 1.2)
 (fround 1.2)
 (fround 1.7)

)


;;;; bitwise operations on integers

;; an integer is represented as a binary number --- a sequence of bits; bitwise
;; operations act on the individual bits of such a sequence

(comment

 ;; shift to left; preserves sign bit
 (ash 7 1)
 (ash 7 -1)
 (ash -5 -2)

 ;; shift to left; does not preserve sign bit (introduces zeros in empty places
 ;; on both sides)
 (lsh 7 1)
 (lsh 7 -1)
 (lsh -5 -2)

 (logand)
 (logand 12 13)
 (logand 14 13 4)

 (logior)
 (logior 12 5)
 (logior 14 13 4)

 (logxor)
 (logxor 12 5)
 (logxor 14 13 4)

 (lognot 5)

 (logcount 5)
 (logcount 0)
 (logcount -1)

)


;;;; standard mathematical functions

(comment

 ;; radians

 pi ;; obsolete
 float-pi
 float-e

 (setq a (/ float-pi 3))

 (sin a)
 (cos a)
 (tan a)

 (asin 1)
 (acos 1)
 (atan 1)
 (atan 3 4)

 (exp 10)

 (log 1000 10)
 (log 512 2)

 (expt 2 8)

 (sqrt 121)

)


;;;; random numbers

;; a deterministic computer program cannot generate true random numbers; a
;; series of pseudo-random numbers is generated in a deterministic fashion ---
;; not truly random, but have statistical properties that mimic a random series
;; (e.g., all values occur equally often)

;; pseudo-random numbers are generated from a seed value; starting from any
;; given seed, the `random' function always generates the same sequence of
;; numbers; Emacs initializes the random seed at startup using the system
;; entropy pool

(comment

 ;; next random number --- different each time
 (random)
 (random 10)

 ;; set a new seed from string's contents
 (random "")
 ;; always the same after reseeding
 (random)

 ;; reset the seed as on Emacs restart
 (random t)
 ;; different each time
 (random)

 ;; note: not the best approach for cryptographic purposes

)
