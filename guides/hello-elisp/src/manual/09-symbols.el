;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; a symbol is an object with a unique name


;;;; symbol components

;; each symbol has four components:
;; - print name
;; - value
;; - function
;; - property list

;; Emacs Lisp is a Lisp-2 --- its symbols have separate cells for values and
;; functions

;; Lisp reader ensures uniqueness of symbol names by interning them and looking
;; for existing symbol before creating a new one

;; symbol's value cell can hold any value; values of certain symbols are
;; predefined and cannot be changed (nil, t, keywords)

;; symbol's function cell usually holds a function or a macro, but can hold a
;; symbol, keyboard macro, keymap, or autoload object


;;;; defining symbols

;; a definition is a Lisp expression that announces your intention to use a
;; symbol in a particular way; itt specifies a value or meaning for the symbol
;; for one kind of use

;; global variables: `defvar': , `defconst', `defcustom'

;; global functions: `defun', `defsubst', `defalias'

;; global macros: `defmacro'

(comment

 (setq foo 123)
 (defun foo (x)
   "Squares X."
   (* x x))

 (symbolp 'foo)

 (symbol-name 'foo)
 (symbol-value 'foo)
 (symbol-function 'foo)
 (symbol-plist 'foo)

 (symbol-function 'comment)

)


;;;; creating and interning symbols

;; symbols are stored (interned) in a hashmap-like structure (obarray) and
;; looked up there when a symbol's name is read; if existing symbol is found, it
;; is used, else a new symbol is created and interned

;; symbols not in obarray are uninterned; the only way to access it is by
;; finding it in some other object or as a variable value; they are sometimes
;; useful in generating code

;; obarray is actually a vector, each element of which is a bucket; its value is
;; either an interned symbol whose name hashes to that bucket, or 0 if the
;; bucket is empty; each interned symbol has a link to the next symbol in the
;; bucket

(comment

 obarray
 (length obarray)

 ;; make sure no such symbol is interned
 (unintern "quux")
 ;; returns uninterned symbol
 (make-symbol "quux")
 ;; does not intern if not already interned
 (intern-soft "quux")

 ;; also returns uninterned symbol
 (gensym)


 ;; creating an obarray
 (setq o1 (make-vector 5 0))

 (intern "bar" o1)
 (intern "foo" o1)
 (intern "quux" o1)
 (intern "frob" o1)

 (eq (intern "foo")
     (intern "foo" o1))

 (mapatoms #'prin1 o1)

)


;;;; symbol properties

;; a symbol can have any number of properties (metadata)


;;;; accessing symbol properties

(comment

 (get 'comment 'custom-prefix)
 (put 'comment 'foo 'bar)

 (setplist 'foo '(type fun doc "None."))
 (symbol-plist 'foo)

 (setq first 'car)
 (function-get 'first 'byte-opcode)
 (get 'car 'byte-opcode)

 (function-put 'first 'foo 'bar)
 (get 'first 'foo)
 (symbol-plist 'first)
 (symbol-plist 'car)

)


;;;; standard symbol properties

;; no code here, see manual


;;;; shorthands

;; shorthands (renamed symbols, abbreviations for symbols) are symbolic forms
;; just like regular symbolic forms, except that the Lisp reader produces
;; symbols with different (usually longer) print name from them

;; since all symbols are stored in a single obarray, symbol names in different
;; packages should be prefixed to avoid clashes, that often produces quite long
;; names; shorthands alleviate that problem


;; example

;; defining shorter names instead of long names; short names will be expanded to
;; long names on read

(add-shorthand! '("snu-" . "some-nice-string-utils-"))

(defun snu-split (separator s &optional omit-nulls)
  "A match-data saving variant of `split-string'."
  (save-match-data (split-string s separator omit-nulls)))

(defun snu-lines (s)
  "Split string S at newline characters into a list of strings."
  (snu-split "\\(\r\n\\|[\n\r]\\)" s))

;; using shorter names in other definitions in this buffer

(defun t-reverse-lines (s)
  (string-join (reverse (snu-lines s)) "\n"))

(comment

 ;; mapping from shorthands to longhands
 read-symbol-shorthands

 'snu-lines
 (symbol-name 'snu-lines)

 (t-reverse-lines "one\ntwo\nthree\n")


 (add-shorthand! '("s/" . "seq-"))
 (s/take [1 2 3 4 5 6 7] 3)
 (remove-shorthand! "s/")
 (seq-take [1 2 3 4 5 6 7] 3)

 (require-with-shorthand 'seq '("seq/" . "seq-"))
 (seq/take [1 2 3 4 5 6 7] 3)

)

;;;; symbols with position

;; a symbol with position is a symbol (the bare symbol), together with an
;; unsigned integer called the position; these are intended for use by the byte
;; compiler to record the position of the symbol occurrence and use it in
;; warning and error messages

(comment

 symbols-with-pos-enabled
 print-symbols-bare

 (setq foo* (position-symbol 'foo 123))

 (symbol-with-pos-p foo*)
 (bare-symbol-p foo*)

 (bare-symbol foo*)
 (symbol-with-pos-pos foo*)

)
