;;; -*- lexical-binding: t; -*-

(load-file "../common.el")


;; a string is an array that contains an ordered sequence of characters

;; a character is a Lisp object which represents a single character of text ---
;; an integer


;;;; string and character basics

;; strings can be manipulated as arrays and sequences; there are two
;; representations of non-ASCII characters in strings (unibyte and multibyte);
;; strings can hold regular expressions; strings can have text properties


;;;; predicates for strings

(comment

 (stringp "foo")
 (string-or-null-p nil)
 (char-or-string-p ?A)

 )


;;;; creating strings

(comment

 (make-string 5 ?x)
 (make-string 0 ?x t)

 (string ?a ?b ?c)

 (substring "abcdefg" 0 3)
 (substring "abcdefg" -3 -1)
 (substring "abcdefg" -3 nil)
 (substring "abcdefg" 0)
 (substring [a b (c) "d"] 1 3)

 (substring-no-properties "abcdefg" 0 3)

 (concat)
 (concat "abc" "def")
 (concat "abc" (list 120 121) [122])
 (concat "abc" nil "-def")

 (split-string "  two words  ")
 (split-string "  two words  " split-string-default-separators)
 (split-string "  two words  " "o" t)
 (split-string "  two words  " "o" t "\s*")
 (split-string "  two words  " "" t "\s*")

 (string-clean-whitespace "  two           words  ")

 (string-trim "  two    words  ")

 (string-trim-left "  two   words  ")

 (string-trim-right "  two   words  ")

 (string-fill "asd asd asd asd asd" 10)

 (string-limit "A quick brown fox jumped over the lazy dog." 10)
 (string-limit "A quick brown fox jumped over the lazy dog." 10 t)

 (string-lines "one\ntwo\nthree\n")

 (string-pad "abc" 10 ?X)
 (string-pad "abc" 10 ?X t)

 (string-chop-newline "abc\ndef\n")

)


;;;; modifying strings

(comment

 ;; array operations (e.g., `aset') can be used

 (setq s1 (copy-sequence "abc"))
 (aset s1 1 ?B)

 (store-substring s1 2 "?")

 (clear-string s1)

)

;;;; comparison of characters and strings

(comment

 (char-equal ?A ?a)
 (let ((case-fold-search nil))
   (char-equal ?A ?a))

 (string= "foo" "foo")
 (string= 'foo 'foo)
 (string-equal "foo" "foo")

 (string-equal-ignore-case "foo" "Foo")

 ;; does not work as expected
 (string-collate-equalp (string ?\uFF40) (string ?\u1FEF) "en_US.UTF-8")


 (string< "abc" "abd")
 (string< "abd" "abc")
 (string< "123" "abc")
 (string< 'foo 'bar)
 (string-lessp "foo" "bar")
 (string-greaterp "foo" "bar")

 (string-collate-lessp "foo" "bar")
 (sort (list "11" "12" "1 1" "1 2" "1.1" "1.2") 'string-collate-lessp)

 (string-version-lessp "foo" "bar")

 (string-prefix-p "foo" "foobar")
 (string-suffix-p "bar" "foobar")

 (string-search "ba" "foobar")

 (compare-strings "foo" 0 2 "fooo" 0 2)

 (string-distance "foo" "fooo")
 (string-distance "foo" "bar")

 (assoc-string :a '((:a 1)))
 (assoc-string :a '(:a 1))

)


;;;; conversion of characters and strings

;; conversions between characters, strings and integers

(comment

 (prin1-to-string [1 2 3])
 (read-from-string "[1 2 3]")

 (number-to-string 256)
 (string-to-number "-123.5")
 ;; no floats in bases other than 10
 (string-to-number "-12a" 12)

 (char-to-string ?a)
 (string-to-char "a")

 ;; vector/list to string
 (concat [1 2 3])
 ;; string to vector
 (vconcat "a b c")
 ;; string to list
 (append "abc" '())

)


;;;; formatting strings

;; substituting values in a format string

(comment

 (format "The '%s' is %d." 'foo 12)
 (format "The '%2$05s' is %1$05d." 12 'foo)

 (format-message "The ``%s'' is %d." 'foo 12)

)


;;;; custom format strings

(comment

 (setq my-site-info
       (list (cons ?s system-name)
             (cons ?t (symbol-name system-type))
             (cons ?c system-configuration)
             (cons ?v emacs-version)
             (cons ?e invocation-name)
             (cons ?p (number-to-string (emacs-pid)))
             (cons ?a user-mail-address)
             (cons ?n user-full-name)))

 (format-spec "%e %v (%c)" my-site-info)
 (format-spec "%n <%a>" my-site-info)

)


;;;; case conversion in Lisp

(comment

 (downcase "Foo")
 (downcase ?X)

 (upcase "Foo")
 (upcase ?x)

 (capitalize "foO")
 (capitalize ?x)

 (upcase-initials "foo bar baz")
 (upcase-initials ?x)

)


;;;; the case table

;; a case table is a char-table that specifies the mapping between upper case
;; and lower case letters; each buffer has a case table (initialized from a
;; standard case table)

;; has 3 derived subtables for reverse mapping (upcase), canonical
;; representations of each letter (canonicalize) and equivalence of letters
;; (equivalences)

(comment

 (case-table-p (standard-case-table))

 ;; for all subsequent buffers
 (standard-case-table)
 (set-standard-case-table (standard-case-table))

 ;; for the current buffer
 (current-case-table)
 (set-case-table (current-case-table))

 (with-case-table (current-case-table)
   (message "howdy"))

 ascii-case-table

 (describe-buffer-case-table)

 ;; better not to mess with it

 ;; (set-case-syntax-pair ?x ?X (current-case-table))
 ;; (upcase ?X)

 ;; borks something most righteously, unmatched delimiter errors all over the
 ;; buffer; also screwed up the bookmarks file
 ;; (set-case-syntax-delims ?o ?o (current-case-table))

 ;; (set-case-syntax ?x "xxx" (current-case-table))

)
