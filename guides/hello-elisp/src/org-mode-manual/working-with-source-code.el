;; [[file:working-with-source-code.org::dot][dot]]
(defun dot (xs ys)
  "Returns dot product of vectors XS and YS."
  (cl-reduce #'+
             (cl-mapcar #'* xs ys)))

(dot '(1 2 3) '(4 5 6))
;; dot ends here
