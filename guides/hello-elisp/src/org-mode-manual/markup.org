#+startup: entitiespretty
#+startup: latexpreview
#+startup: inlineimages


* Paragraphs

  Paragraphs are separated by at least one empty line. \\ If you need to
  enforce a line break within a paragraph, use ~\\~ at the end of a line.

  Verses preserve line breaks, indentation and blank lines:
  #+begin_verse
    Great clouds overhead
    Tiny black birds rise and fall
    Snow covers Emacs

      ---AlexSchroeder
  #+end_verse

  Quotes are indented on both the left and the right margin:
  #+begin_quote
  Everything should be made as simple as possible,
  but not any simpler ---Albert Einstein
  #+end_quote

  Centering is done like this:
  #+begin_center
  Everything should be made as simple as possible, \\
  but not any simpler
  #+end_center

* Emphasis

  Words can be
  - *bold*
  - /italic/
  - _underlined_
  - =verbatim=
  - ~code~
  - +strike-through+

  When marked text contains marker characters itself, the result may be not
  quite right:

  /One may expect this whole sentence to be italicized, but the following
  ~user/?variable~ contains =/= character, which effectively stops emphasis
  there./

  A zero width space can be used to resolve such ambiguities (~C-h u <RET>
  zero width space<RET>~):

  /One may expect this whole sentence to be italicized, but the following
  ~user​/​?variable~ contains =/= character, which effectively stops emphasis
  there./

* Subscripts and Superscripts

  The radius of the sun is R_sun = 6.96 x 10^8 m.  On the other hand, the
  radius of Alpha Centauri is R_{Alpha Centauri} = 1.28 x R_{sun}.

  Toggle pretty entities: ~C-c C-x \~ (org-toggle-pretty-entities)

* Special Symbols

  Pro tip: Given a circle \Gamma of diameter d, the length of its circumference is
  \pi{}d.

* Embedded LaTeX
** LaTeX Fragments

   \begin{equation}
   x=\sqrt{b}
   \end{equation}

   If $a^2=b$ and \( b=2 \), then the solution must be
   either $$ a=+\sqrt{2} $$ or \[ a=-\sqrt{2} \]

   Preview: ~C-c C-x C-l~ (org-latex-preview)

* Literal Examples

  Typeset in monospace:
  #+begin_example
  Some example from a text file.
  #+end_example

  Some syntax should be quoted with comma:
  #+begin_example
  ,#+startup: latexpreview
  ,* I am no real headline
  #+end_example

  Source code:

  #+name: xor
  #+begin_src elisp -n :var a='t :var b='nil
    ;; lines are numbered
    (defun org-xor (a b)
      "Exclusive or."
      (if a (not b) b))           (ref:xor)

    (org-xor a b)
  #+end_src

  #+RESULTS: xor
  : t

  References:
  #+begin_src emacs-lisp -n -r
    ;; labels will be removed
    (save-excursion                 (ref:sc)
       (goto-char (point-min))      (ref:jump)
  #+end_src

  In line [[(sc)]] we remember the current position. [[(jump)][Line (jump)]] jumps to
  point-min. See also [[(xor)]].

  ~C-c '~ (org-edit-special) to edit the block in a separate buffer.


* Images

  An image is a link to an image file without a description part:
  #+caption[Short caption.]: Longer caption (for link or table).
  #+name:   fig:logo
  #+attr_html: :width 100px
  #+attr_latex: :width .3\textwidth
  [[file:~/Documents/workspace/el/el.png]]

  Show inline images: ~C-c C-x C-v~ (org-toggle-inline-images)

* Horizontal Rules

  A line with at least 5 dashes:
  ------------

* Creating Footnotes

  Headline with a name "Footnotes" has a special meaning by default --- it
  stores footnotes from entire document.

  The Org website[fn:1] now looks a lot better than it used to.

* Footnotes

  No indentation allowed.

[fn:1] The link is: https://orgmode.org
