#!/usr/bin/env bb

(let [xs '[2 4]
      ys '[6 8]]
(defn dot
  "Returns dot product of vectors `xs` and `ys`."
  [xs ys]
  (reduce + (map * xs ys)))

(dot xs ys))
