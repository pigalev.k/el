;; C-x C-e P in an .org file to publish all the .org files in the current
;; directory


(setq org-publish-project-alist
      '(("org-mode-manual"
         :base-directory "."
         :publishing-function org-html-publish-to-html
         :publishing-directory "./published"
         :section-numbers nil
         :with-toc nil
         :html-head "<link rel=\"stylesheet\"
                    href=\"./style.css\"
                    type=\"text/css\"/>")))
