(load-file "common.el")


;; `use-package' accepts the name of a library (feature); there can be many in
;; one package, and they may require separate calls to `use-package' to load
;; (if they don't load each other)


;; anatomy of a `use-package' call: set point before the form and do M-x
;; `emacs-lisp-macroexpand'; or set point after the form and do M-x
;; `pp-macroexpand-last-sexp'

(comment

 (use-package foo
   :commands (foo bar baz)
   :init (exec-init 'a)
   :config (exec-config 'b)
   :custom (set-option 'c :val)
   :bind ("C-c y" . exec-bind)
   :hook (prog-mode . foo-mode))

)


;;;; loading packages

;; when called only with a library name, `use-package' will load (`require')
;; it immediately; autoloading keywords will set up autoloading for their
;; arguments and defer actual loading of the package: `hook', `:commands',
;; `:bind', `:bind*', `:bind-keymap', `:bind-keymap*' `:mode', `:interpreter';
;; even without them, loading can be explicitly deferred with `defer' (t ---
;; until some code from the package will be called, number --- after so many
;; seconds)

(comment

 ;; load immediately

 (use-package foo)

 (use-package foo
   :demand t)

 ;; load later

 (use-package foo
   :defer t)

 (use-package foo
   :defer 180)

 (use-package foo
   :commands (foo-command))

 (use-package foo
   :bind ("C-c y" . foo-command))

 ;; do not load (expands to nil)

 (use-package foo
   :disabled)

)


;; conditional loading

(comment

 ;; `:if' and `:when' are the same; `:unless' is the opposite

 (use-package foo
   :if (display-graphic-p))

 (use-package foo
   :if (eq system-type 'gnu/linux))

 (use-package foo
   :unless (package-installed-p 'bar))

 (use-package foo
   :unless (locate-library "bar.el"))

 ;; decide even before installing
 (when (memq window-system '(mac ns))
  (use-package foo
    :ensure t))

)


;; loading packages in sequence

(comment

 (use-package foo)
 (use-package bar)
 (use-package baz)

 (use-package quux
   :after (foo bar baz))

 (use-package quux
   :after (:any foo bar baz))

 (use-package quux
   :after (:all (:any foo bar) (:any bar baz)))

)


;; prevent loading if dependencies are missing

(comment

 ;; equivalent

 (use-package foo
   :requires (bar baz))

 (use-package foo
   :if (and (featurep 'bar)
            (featurep 'baz)))

)


;; custom `load-path' and autoloads (for manually installed packages)

(comment

 (use-package foo
   :load-path "dir/foo"
   :autoload foo-command)

)


;;;; configuring packages

;; `:preface' is evaluated first (at load time and at compilation time); then
;; `:init'; then package loads; then `:config'

(comment

 (use-package foo
   :preface 'a
   :init 'b
   :config 'c)

)


;; key bindings

;; see all keys (re)bound by `use-package': `describe-personal-keybindings'

(comment

 ;; global

 (use-package foo
   :bind (("C-c y" . foo-command)
          ("C-c x" . foo-other-command)
          ([f10] . foo-mode)))

 ;; local

 (use-package foo
   :bind (:map foo-mode-map
          ("C-c y" . foo-command)
          :map bar-mode-map
          ("C-c x" . foo-other-command)))

 ;; both

 (use-package foo
   :bind (([f10] . foo-mode)
          :map foo-mode-map
          ("C-c y" . foo-command)
          ("C-c x" . foo-other-command)))

 ;; a keymap instead of a command

 (use-package foo
   :bind-keymap ("C-c y" . foo-command-map))

 ;; repeat maps (not yet)

)


;; hooks

;; rule of thumb: when adding hooks, the package being configured should
;; provide the command(s); hook names should come from dependent packages

(comment

 ;; the same

 (use-package foo
  :commands foo-mode
  :init
  (add-hook 'prog-mode-hook #'foo-mode))

 (use-package foo
   :hook (prog-mode . foo-mode))

 ;; not quite the same (does not add '-mode' to 'foo'), although the manual
 ;; says it should

 (use-package foo
   :hook prog-mode)

 ;; several hooks; the same

 (use-package company
   :hook ((prog-mode text-mode) . company-mode))

 (use-package company
   :hook ((prog-mode . company-mode)
          (text-mode . company-mode)))

 ;; again, not quite the same

 (use-package foo
   :hook (prog-mode text-mode))

)


;; modes and interpreters

(comment

 ;; extension is ".foo" or shebang contains "foo"; handled by `foo-mode'
 ;; command

 (use-package foo-mode
   :mode "\\.foo\\'"
   :interpreter "foo")

 ;; if the package name is different from `foo-mode'

 (use-package foo
   :mode ("\\.foo\\'" . foo-mode)
   :interpreter ("foo" . foo-mode))

 ;; several extensions/shebang patterns for one mode

 (use-package foo-mode
   :mode ("\\.foo\\'" "\\.bar\\'")
   :interpreter ("foo" "bar"))

)


;; magic handlers (match against beginning of a buffer)

(comment

 ;; higher priority than `:mode'

 (use-package foo
   :magic ("%FOO" . foo-view-mode))

 ;; lower priority than `:mode'

 (use-package foo
   :mode ("\\.foo\\'" . foo-mode)
   :magic-fallback ("%FOO" . foo-view-mode))

)


;; user options and faces

(comment

 ;; `setopt' can be used in `:config' instead

 (use-package foo
   :custom (foo-size 123))

 (use-package foo
  :custom-face
  (foo-standard-face ((t (:slant italic)))))

)


;; hide/replace minor mode names from modeline

(comment

 ;; install and use one of the two

 (use-package diminish)

 ;; replace

 (use-package foo
   :diminish (foo-mode . "f"))

 ;; hide; the same

 (use-package foo
   :diminish foo-mode)

 (use-package foo-mode
   :diminish)


 (use-package delight)

 ;; hide; the same

 (use-package foo-mode
   :delight)

 (use-package foo
   :delight foo-mode)

 ;; replacement is somewhat complex, so omit it for now

)


;;;; installing packages automatically

;; seems like `straight-use-package' is always called if `straight.el' is used
;; as a package manager

(comment

 ;; install if not already present

 (use-package tex
   :ensure t)

 ;; if package name differs from the library name (seems like it does not
 ;; matter with `straight.el', always uses `tex')

 (use-package tex
   :ensure auctex)

)


;; pinning packages (probably does not matter with `straight.el')

(comment

 ;; pin to ELPA

 (use-package foo
   :pin gnu)

 ;; manual updates

 (use-package foo
   :pin manual)

)


;;;; byte-compiling the init file

;; not recommended

(comment

 ;; declare variables to silence compilation warnings about undefined (yet)
 ;; variables or functions

 (use-package foo
   :defines foo-list
   :functions foo-command
   :commands foo-mode)

 ;; do not require at compile time

 (use-package foo
   :no-require t)

)


;;;; troubleshooting


;; handling loading errors

(comment

 ;; `straight.el' will blow up anyway if this example is evaled

 (use-package foo
   ;; errors are never trapped in the preface, since doing so would hide
   ;; definitions from the byte-compiler
   :preface (message "I'm here at byte-compile and load time")
   :init (message "I'm always here at startup")
   :config
   (message "I'm always here after the package is loaded")
   (error "oops")
   ;; don't try to (require 'example), this is just an example!
   :no-require t
   ;; providing custom error handler; t or nil will enable or disable error
   ;; catching at load time
   :catch (lambda (keyword err)
            (message (error-message-string err))))

)


;; gathering statistics

;; set `use-package-compute-statistics' to non-nil, restart Emacs, do M-x
;; `use-package-report'


;; system packages can be installed with `:use-package-ensure-system-package';
;; will not go into it here

;; new keywords can be added; see manual

(comment

 use-package-keywords

)
