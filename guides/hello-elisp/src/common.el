;; common helpers

(defmacro comment (&rest body)
  "Comment out one or more expressions."
  nil)

(defun add-shorthand! (shorthand)
  "Establishes a short prefix for use instead of the long prefix for
symbol names in the current buffer. SHORTHAND is a pair of short
and long prefix strings."
  (make-local-variable 'read-symbol-shorthands)
  (add-to-list 'read-symbol-shorthands shorthand))

(defun remove-shorthand! (short)
  "Removes existing shorthand SHORT for symbol names in the current
buffer."
  (setq read-symbol-shorthands
        (assoc-delete-all short read-symbol-shorthands)))

(defun require-with-shorthand (feature shorthand)
  "Requires FEATURE and establishes SHORTHAND for symbol names in
the current buffer. SHORTHAND is a pair of short and long prefix
strings."
  (require feature)
  (add-shorthand! shorthand))
