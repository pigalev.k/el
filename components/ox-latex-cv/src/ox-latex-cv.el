;;; ox-latex-cv.el --- LaTeX CV Back-End for Org Export Engine
;; -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Constantine Pigalev

;; Author: Constantine Pigalev <pigalev.k@gmail.com>
;; Maintainer: Constantine Pigalev <pigalev.k@gmail.com>
;; Keywords: outlines, org, latex, cv, resume

;; This file is NOT a part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; TODO

;;; Code:

(require 'ox-latex)


;;; Define Back-End

(org-export-define-derived-backend 'latex-cv 'latex
  :options-alist
  '(
    ;; (:latex-class "LATEX_CLASS" nil "moderncv" t)
    (:cvtemplate "CVTEMPLATE" nil "moderncv" t)
    (:cvstyle "CVSTYLE" nil "classic" t)
    (:cvcolor "CVCOLOR" nil nil t)
    (:mobile "MOBILE" nil nil parse)
    (:homepage "HOMEPAGE" nil nil parse)
    (:address "ADDRESS" nil nil newline)
    (:photo "PHOTO" nil nil parse)
    (:gitlab "GITLAB" nil nil parse)
    (:github "GITHUB" nil nil parse)
    (:linkedin "LINKEDIN" nil nil parse)
    (:with-email nil "email" t t))
  :menu-entry
  '(?C "Export as CV (LaTeX)"
       ((?L "As LaTeX buffer" org-latex-export-as-latex)
	(?l "As LaTeX file" org-latex-export-to-latex)
	(?p "As PDF file" org-latex-export-to-pdf)
	(?o "As PDF file and open"
	    (lambda (a s v b)
	      (if a (org-latex-export-to-pdf t s v b)
		(org-open-file (org-latex-export-to-pdf nil s v b))))))))

(provide 'ox-latex-cv)

;;; ox-latex-cv.el ends here
