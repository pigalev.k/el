# nuts-and-bolts

A collection of small convenience functions.

## Getting started

Put the `.el` files in the Emacs load path and require it. Create keybindings
for the desired functions.

Example:

```
(use-package nuts-and-bolts
  :straight
  (nuts-and-bolts :type git
                       :host gitlab
                       :repo "pigalev.k/el"
                       :local-repo "pk-el"
                       :depth 1
                       :files ("components/nuts-and-bolts/*.el"))
  :bind ("C-<backspace>" . nuts-and-bolts/delete-backward-word))
```
