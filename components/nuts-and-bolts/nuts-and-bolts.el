;;; nuts-and-bolts.el --- Small convenience functions.  -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Constantine Pigalev

;; Author: Constantine Pigalev <pigalev.k@gmail.com>
;; Keywords: convenience, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; No commentaries.

;;; Code:


;; https://emacs.stackexchange.com/a/37680
(defun nuts-and-bolts/delete-backward-word ()
  "Deletes word backward without placing it to the kill ring."
  (interactive "*")
  (push-mark)
  (backward-word)
  (delete-region (point) (mark)))


(provide 'nuts-and-bolts)
;;; nuts-and-bolts.el ends here
