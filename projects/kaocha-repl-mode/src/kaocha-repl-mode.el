;;; kaocha-repl-mode.el --- Interaction with Kaocha test runner from REPL -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Constantine Pigalev

;; Author: Constantine Pigalev <pigalev.k@gmail.com>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A minor mode for interacting with Kaocha test runner from REPL (running
;; tests) in the Clojure/CIDER project.


;;; Code:


(require 'cider)


;;;; domain functions

(defun kaocha-repl-mode-run-tests-unit ()
  "Runs all unit tests in the project with Kaocha."
  (interactive)
  (cider-interactive-eval "(require '[kaocha.repl])")
  (cider-interactive-eval "(kaocha.repl/run :unit)"))

(defun kaocha-repl-mode-run-tests-all ()
  "Runs all tests of all suites in the project with Kaocha."
  (interactive)
  (cider-interactive-eval "(require '[kaocha.repl])")
  (cider-interactive-eval "(kaocha.repl/run-all)"))


;;;; minor mode plumbing

(defvar kaocha-repl-mode-lighter " Ka"
  "Modeline label for kaocha-repl-mode.")

(defvar kaocha-repl-mode-command-map-prefix "C-c k"
  "Prefix for kaocha-repl-mode commands.")

(defvar kaocha-repl-mode-command-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "t") #'kaocha-repl-mode-run-tests-unit)
    (define-key map (kbd "T") #'kaocha-repl-mode-run-tests-all)
    map)
  "Keymap for kaocha-repl-mode commands, bound to
`kaocha-repl-mode-keymap-prefix'.")
(fset 'kaocha-repl-mode-command-map kaocha-repl-mode-command-map)

(defvar kaocha-repl-mode-map
  (let ((map (make-sparse-keymap)))
    (easy-menu-define kaocha-repl-mode-menu map
      "Menu for kaocha-repl-mode"
      '("Kaocha tests" :visible t
        ["Run unit tests" kaocha-repl-mode-run-tests-unit]
        ["Run all tests" kaocha-repl-mode-run-tests-all]))
    map)
  "Keymap for kaocha-repl-mode.")

;;;###autoload
(define-minor-mode kaocha-repl-mode
  "A minor mode for interacting with Kaocha test runner from
REPL (running tests) in the Clojure/CIDER project."
  :lighter kaocha-repl-mode-lighter
  :keymap kaocha-repl-mode-map
  (cond
   (kaocha-repl-mode
    (when kaocha-repl-mode-command-map-prefix
      (define-key kaocha-repl-mode-map
                  (kbd kaocha-repl-mode-command-map-prefix)
                  'kaocha-repl-mode-command-map)))
   (t nil)))


(provide 'kaocha-repl-mode)
;;; kaocha-repl-mode.el ends here
