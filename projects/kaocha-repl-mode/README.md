# kaocha-repl-mode

A minor mode for interacting with
[Kaocha](https://github.com/lambdaisland/kaocha) test runner from REPL
(running tests) in the Clojure/CIDER project.

## Getting Started

Put `kaocha-repl-mode.el` in the Emacs load path and require it.

Example:

```
(use-package kaocha-repl-mode
  :straight
  (kaocha-repl-mode :type git
                    :host gitlab
                    :repo "pigalev.k/el"
                    :local-repo "pk-el"
                    :depth 1
                    :files ("projects/kaocha-repl-mode/src/*.el"))
  :hook
  (cider-mode . kaocha-repl-mode))
```

## Usage

Mode commands:

- `t`: run unit tests
- `T`: run all tests

## Configuration

- `kaocha-repl-mode-command-map-prefix`: prefix to place the mode commands
  under; default is `C-c k`.
