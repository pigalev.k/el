# integrant-repl-mode

A minor mode for interacting with
[Integrant](https://github.com/weavejester/integrant) [development
REPL](https://github.com/weavejester/integrant-repl) (managing Integrant
systems in development mode --- starting, stopping, resetting, ...) in the
Clojure/CIDER project.

## Getting Started

Put `integrant-repl-mode.el` in the Emacs load path and require it.

Example:

```
(use-package integrant-repl-mode
  :straight
  (integrant-repl-mode :type git
                       :host gitlab
                       :repo "pigalev.k/el"
                       :local-repo "pk-el"
                       :depth 1
                       :files ("projects/integrant-repl-mode/src/*.el"))
  :hook
  (cider-mode . integrant-repl-mode))
```

## Usage

Mode commands:

- `i`: requires and initializes Integrant development REPL
- `g`: starts the Integrant system
- `c`: stops the Integrant system and clears its configuration
- `h`: stops the Integrant system
- `r`: stops the Integrant system, reloads its changed namespaces and starts
  it again
- `R`: stops the Integrant system, reloads all its namespaces and starts it
  again

## Configuration

- `integrant-repl-mode-command-map-prefix`: prefix to place the mode commands
  under; default is `C-c i`.
