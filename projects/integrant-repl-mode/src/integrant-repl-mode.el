;;; integrant-repl-mode.el --- Interaction with Integrant REPL -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Constantine Pigalev

;; Author: Constantine Pigalev <pigalev.k@gmail.com>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A minor mode for interacting with Integrant development REPL (managing
;; Integrant systems in development mode --- starting, stopping, resetting,
;; ...) in the Clojure project.

;;; Code:


(require 'cider)


;;;; domain functions

(defun integrant-repl-mode-run-command (ig-repl-command)
  "Runs an Integrant REPL command (go, reset, reset-all, halt,
clear, ...)."
  (let* ((ig-repl-command-expr (format "(integrant.repl/%s)" ig-repl-command)))
    (cider-interactive-eval ig-repl-command-expr)))

(defun integrant-repl-mode-init ()
  "Requires and initializes Integrant development REPL.
Code that prepares system to run should be in `user' namespace,
so it can be loaded automatically on development REPL startup."
  (interactive)
  (cider-interactive-eval "(require 'integrant.repl)"))

(defun integrant-repl-mode-go ()
  "Starts the Integrant system."
  (interactive)
  (integrant-repl-mode-run-command "go"))

(defun integrant-repl-mode-clear ()
  "Stops the Integrant system and clears its configuration."
  (interactive)
  (integrant-repl-mode-run-command "clear"))

(defun integrant-repl-mode-halt ()
  "Stops the Integrant system."
  (interactive)
  (integrant-repl-mode-run-command "halt"))

(defun integrant-repl-mode-reset ()
  "Stops the Integrant system, reloads its changed namespaces and
starts it again."
  (interactive)
  (integrant-repl-mode-run-command "reset"))

(defun integrant-repl-mode-reset-all ()
  "Stops the Integrant system, reloads all its namespaces and starts
it again."
  (interactive)
  (integrant-repl-mode-run-command "reset-all"))


;;;; minor mode plumbing

(defvar integrant-repl-mode-lighter " Ig"
  "Modeline label for integrant-repl-mode.")

(defvar integrant-repl-mode-command-map-prefix "C-c i"
  "Prefix for integrant-repl-mode commands.")

(defvar integrant-repl-mode-command-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "i") #'integrant-repl-mode-init)
    (define-key map (kbd "g") #'integrant-repl-mode-go)
    (define-key map (kbd "c") #'integrant-repl-mode-clear)
    (define-key map (kbd "h") #'integrant-repl-mode-halt)
    (define-key map (kbd "r") #'integrant-repl-mode-reset)
    (define-key map (kbd "R") #'integrant-repl-mode-reset-all)
    map)
  "Keymap for integrant-repl-mode commands, bound to
`integrant-repl-mode-keymap-prefix'.")
(fset 'integrant-repl-mode-command-map integrant-repl-mode-command-map)

(defvar integrant-repl-mode-map
  (let ((map (make-sparse-keymap)))
    (easy-menu-define integrant-repl-mode-menu map
      "Menu for integrant-repl-mode"
      '("Integrant REPL" :visible t
        ["Init" integrant-repl-mode-init]
        ["Go" integrant-repl-mode-go]
        ["Clear" integrant-repl-mode-clear]
        ["Halt" integrant-repl-mode-halt]
        ["Reset" integrant-repl-mode-reset]
        ["Reset All" integrant-repl-mode-reset-all]))
    map)
  "Keymap for integrant-repl-mode.")

;;;###autoload
(define-minor-mode integrant-repl-mode
  "A minor mode for interacting with Integrant development
REPL (managing Integrant systems in development mode ---
starting, stopping, resetting, ...) in the Clojure/CIDER project."
  :lighter integrant-repl-mode-lighter
  :keymap integrant-repl-mode-map
  (cond
   (integrant-repl-mode
    (when integrant-repl-mode-command-map-prefix
      (define-key integrant-repl-mode-map
                  (kbd integrant-repl-mode-command-map-prefix)
                  'integrant-repl-mode-command-map)))
   (t nil)))


(provide 'integrant-repl-mode)
;;; integrant-repl-mode.el ends here
